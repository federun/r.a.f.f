import 'package:cloud_firestore/cloud_firestore.dart';

/*
Service for user collection on firestore
*/
class DatabaseService {
  final String uid;
  DatabaseService({this.uid});

  //collection reference
  final CollectionReference utentiCollection =
      FirebaseFirestore.instance.collection('utente');

  Future insertUserData(
    bool venditore,
    String denominazioneSociale,
    String indirizzo,
    String nomeAttivita,
    String partitaIva,
  ) async {
    if (venditore == true) {
      return await utentiCollection.doc(uid).set({
        'venditore': {
          'denominazioneSociale': denominazioneSociale,
          'indirizzo': indirizzo,
          'nomeAttivita': nomeAttivita,
          'partitaIva': partitaIva,
        },
        'prodottiCarrello': {},
      });
    } else {
      return await utentiCollection.doc(uid).set({
        'prodottiCarrello': {},
      });
    }
  }

  Future deleteuser() {
    return utentiCollection.doc(uid).delete();
  }
}
