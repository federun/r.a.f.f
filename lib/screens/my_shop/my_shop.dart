import 'package:flutter/material.dart';
import 'package:raaf_project/models/UserApp.dart';
import 'package:raaf_project/models/Seller.dart';
import 'package:raaf_project/screens/my_shop/add_product/add_product.dart';
import 'package:raaf_project/services/seller.dart';
import 'package:raaf_project/shared/drawer.dart';
import 'package:raaf_project/shared/keys.dart';

class MyShop extends StatefulWidget {
  static String routeName = "/MyShop";

  final UserApp user;
  MyShop({this.user});

  @override
  _MyShopState createState() => _MyShopState();
}

class _MyShopState extends State<MyShop> {
  final SellerService _sellerService = SellerService();

  Future _sellerFtuture;

  @override
  void initState() {
    super.initState();
    _sellerFtuture = _sellerService.sellerInfo(uid: widget.user.uid);
  }

  _addProduct(Seller seller) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => AddProduct(
                user: widget.user,
                seller: seller,
              )),
    );
    if (result != null) {
      ScaffoldMessenger.of(context)
        ..removeCurrentSnackBar()
        ..showSnackBar(SnackBar(content: Text(result)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          key: Key(Keys.pageMyShop),
          toolbarHeight: 60,
          backgroundColor: Colors.blue,
          elevation: 0.0,
          title: Text("Il mio negozio"),
        ),
        drawer: RaffDrawer(),
        resizeToAvoidBottomInset: false,
        body: FutureBuilder(
          future: _sellerFtuture,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                Seller _seller = snapshot.data;

                return Padding(
                  padding: EdgeInsets.all(20),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Attivita",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 20.0),
                      Center(
                          // here only return is missing
                          child: Text(
                        snapshot.data.activityName,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      )),
                      SizedBox(height: 15.0),
                      Flexible(
                          child: GridView.count(
                        primary: false,
                        crossAxisSpacing: 10,
                        mainAxisSpacing: 10,
                        crossAxisCount: 2,
                        children: <Widget>[
                          Container(
                              padding: const EdgeInsets.all(8),
                              child: TextButton(
                                key: Key(Keys.buttonAddProduct),
                                onPressed: () {
                                  _addProduct(_seller); // another file
                                },
                                child: Column(
                                  children: [
                                    Icon(
                                      Icons.add_business,
                                      size: 90,
                                    ),
                                    Text("Aggiungi Prodotto",
                                        textAlign: TextAlign.center)
                                  ],
                                ),
                              )),
                          Container(
                              padding: const EdgeInsets.all(8),
                              child: TextButton(
                                onPressed: () => print("remove pressed"),
                                child: Column(
                                  children: [
                                    Icon(
                                      Icons.highlight_remove_outlined,
                                      size: 90,
                                    ),
                                    Text("Rimuovi Prodotto",
                                        textAlign: TextAlign.center)
                                  ],
                                ),
                              )),
                          Container(
                              padding: const EdgeInsets.all(8),
                              child: TextButton(
                                  onPressed: () => print("edit pressed"),
                                  child: Column(
                                    children: [
                                      Icon(
                                        Icons.mode_edit,
                                        size: 90,
                                      ),
                                      Text("Modifica Prodotto",
                                          textAlign: TextAlign.center)
                                    ],
                                  ))),
                          Container(
                              padding: const EdgeInsets.all(8),
                              child: TextButton(
                                onPressed: () => print("statistics pressed"),
                                child: Column(
                                  children: [
                                    Icon(
                                      Icons.bar_chart,
                                      size: 90,
                                    ),
                                    Text("Statistiche Acquisti",
                                        textAlign: TextAlign.center)
                                  ],
                                ),
                              )),
                          Container(
                              padding: const EdgeInsets.all(8),
                              child: TextButton(
                                onPressed: () =>
                                    print("statistics pressed (views)"),
                                child: Column(
                                  children: [
                                    Icon(
                                      Icons.stacked_line_chart_sharp,
                                      size: 90,
                                    ),
                                    Text("Statistiche visualizzazioni",
                                        textAlign: TextAlign.center)
                                  ],
                                ),
                              )),
                          Container(
                              padding: const EdgeInsets.all(8),
                              child: TextButton(
                                onPressed: () => print("info pressed"),
                                child: Column(
                                  children: [
                                    Icon(
                                      Icons.business_center_rounded,
                                      size: 90,
                                    ),
                                    Text("Informazioni Negozio",
                                        textAlign: TextAlign.center),
                                  ],
                                ),
                              )),
                        ],
                      )),
                    ],
                  ),
                );
              }
            } else if (snapshot.hasError) {
              print("Error, no data avaiable");
            }

            return Center(child: CircularProgressIndicator());
          },
        ));
  }
}
