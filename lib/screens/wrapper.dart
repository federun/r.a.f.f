import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:raaf_project/models/UserApp.dart';
import 'package:raaf_project/screens/authenticate/authenticate.dart';
import 'package:raaf_project/screens/home/home.dart';

class Wrapper extends StatelessWidget {
  static String routeName = "/";
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserApp>(context);

    if (user == null) {
      return Authenticate();
    } else {
      return Home(user: user);
    }
    //return either Home or Autenticate widget
  }
}
