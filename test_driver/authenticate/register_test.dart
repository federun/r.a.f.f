// Imports the Flutter Driver API.
import 'package:flutter_driver/flutter_driver.dart';
import 'package:raaf_project/shared/keys.dart';
import 'package:test/test.dart';

Future<void> main() async {
  FlutterDriver driver;
  Future<void> delay([int milliseconds = 250]) async {
    await Future<void>.delayed(Duration(milliseconds: milliseconds));
  }

  // Connect to the Flutter driver before running any tests.
  setUpAll(() async {
    driver = await FlutterDriver.connect();
  });

  tearDownAll(() async {
    if (driver != null) {
      driver.close();
    }
  });
  test('check flutter driver health', () async {
    final health = await driver.checkHealth();
    expect(health.status, HealthStatus.ok);
  });

  group('signin', () {
    final String userMail = 'email@email.com';
    final String userPassword = 'aaaaaa';
    final String userName = 'Mario';
    final String userSurname = 'Rossi';
    final String companyName = 'negozio srl';
    final String address = 'via di qua 7b';
    final String businnessName = 'vestiti qui';
    final String vatNumber = '1234';

    Future premiFlag(SerializableFinder flag) async {
      await driver.waitFor(flag);
      await driver.scrollIntoView(flag);
      await driver.tap(flag);
    }

    Future pressButton(
        SerializableFinder iscrivitiButton, bool iscrizioneOk) async {
      await driver.waitFor(iscrivitiButton);
      await driver.scrollIntoView(iscrivitiButton);
      // premo pulsante di iscrizione
      await driver.tap(iscrivitiButton);
      await delay(5000);
      if (iscrizioneOk == false) {
        // aspetto 5 secondi e controllo se trovo ancora il pulsante di iscrizione
        await driver.waitFor(iscrivitiButton);
      } else {
        // aspetto 5 secondi e controllo se mi trovo nella home page
        final homePage = find.byValueKey(Keys.homePage);
        await driver.waitFor(homePage);
      }
    }

    Future insertText(SerializableFinder field, String testo) async {
      await driver.waitFor(field);
      await driver.scrollIntoView(field);
      await driver.tap(field);
      await driver.enterText(testo);
      await driver.waitFor(find.text(testo));
    }

    Future eliminaAccount(bool accesso) async {
      if (accesso == false) {
        // inserimento email
        final emailField = find.byValueKey(Keys.emailField);
        await insertText(emailField, userMail);

        // inserimento password
        final passwordField = find.byValueKey(Keys.passwordField);
        await insertText(passwordField, userPassword);

        // cerca il bottone di accesso e lo premo
        final accediButton = find.byValueKey(Keys.accediButton);
        await pressButton(accediButton, true);
      }
      final drawer = find.byTooltip('Open navigation menu');
      await driver.tap(drawer);

      final rimuoviAccountButton = find.byValueKey(Keys.rimuoviAccountButton);
      await driver.tap(rimuoviAccountButton);

      final passwordField = find.byValueKey(Keys.passwordField);
      await insertText(passwordField, userPassword);

      final continueButton = find.byValueKey(Keys.continueButton);
      await driver.waitFor(continueButton);
      await delay(2000);
      await driver.tap(continueButton);
    }

    test('iscriviti con campi vuoti', () async {
      // premo il pulsante di iscriviti senza inserire campi
      //mi aspetto di rimanere nella pagina di registrazione

      //  elimino l'account creato per i test
      await eliminaAccount(false);
      // passo alla pagina di registrazione
      final toIscrivitiPage = find.byValueKey(Keys.toIscrivitiPage);
      await driver.waitFor(toIscrivitiPage);
      await driver.tap(toIscrivitiPage);

      //accetto le condizioni
      final accettoCondizioniFlag = find.byValueKey(Keys.accettoCondizioniFlag);
      await premiFlag(accettoCondizioniFlag);

      // cerca il bottone di iscrizione
      final iscrivitiButton = find.byValueKey(Keys.iscrivitiButton);
      await pressButton(iscrivitiButton, false);
    });
    test('isciviti inserendo solo un campo richiesto', () async {
      // premo il pulsante di iscriviti senza inserire tutti i campi
      //mi aspetto di rimanere nella pagina di registrazione
      //----
      // inserisco solo la mail
      final emailField = find.byValueKey(Keys.emailField);
      await insertText(emailField, userMail);

      // cerca il bottone di iscrizione
      final iscrivitiButton = find.byValueKey(Keys.iscrivitiButton);
      await pressButton(iscrivitiButton, false);

      //pulisco la mail
      await insertText(emailField, '');

      //----
      //inserisco solo la password
      final passwordField = find.byValueKey(Keys.passwordField);
      await insertText(passwordField, userPassword);

      await pressButton(iscrivitiButton, false);

      //pulisco la password
      await insertText(passwordField, '');

      //----
      //inserisco solo la conferma della password
      final confermaPasswordField = find.byValueKey(Keys.confermaPasswordField);
      await insertText(confermaPasswordField, userPassword);

      await pressButton(iscrivitiButton, false);

      //pulisco la password
      await insertText(confermaPasswordField, '');

      //----
      //inserisco solo il nome
      final nomeField = find.byValueKey(Keys.nomeField);
      await insertText(nomeField, userName);

      await pressButton(iscrivitiButton, false);

      //pulisco il nome
      await insertText(nomeField, '');

      //----
      //inserisco solo il cognome
      final cognomeField = find.byValueKey(Keys.cognomeField);
      await insertText(cognomeField, userSurname);

      await pressButton(iscrivitiButton, false);

      //pulisco il cognome
      await insertText(cognomeField, '');

      //----
      //seleziono sono un venditore e inserisco solo denominazione sociale
      final sonoVenditoreFlag = find.byValueKey(Keys.sonoVenditoreFlag);
      await premiFlag(sonoVenditoreFlag);

      final denominazioneSocialeField =
          find.byValueKey(Keys.denominazioneSocialeField);
      await insertText(denominazioneSocialeField, companyName);

      await pressButton(iscrivitiButton, false);

      //pulisco il campo
      await insertText(denominazioneSocialeField, '');

      //----
      //inserisco solo l indirizzo
      final indirizzoField = find.byValueKey(Keys.indirizzoField);
      await insertText(indirizzoField, address);

      await pressButton(iscrivitiButton, false);

      //pulisco il campo
      await insertText(indirizzoField, '');

      //----
      //inserisco solo il nome dell attivita
      final nomeAttivitaField = find.byValueKey(Keys.nomeAttivitaField);
      await insertText(nomeAttivitaField, businnessName);

      await pressButton(iscrivitiButton, false);

      //pulisco il campo
      await insertText(nomeAttivitaField, '');

      //----
      //inserisco solo il nome dell attivita
      final partitaIvaField = find.byValueKey(Keys.partitaIvaField);
      await insertText(partitaIvaField, vatNumber);

      await pressButton(iscrivitiButton, false);

      //pulisco il campo
      await insertText(partitaIvaField, '');

      //disattivo venditore
      await premiFlag(sonoVenditoreFlag);
    });
    test('isciviti come compratore inserendo tutti tranne un campo richiesto',
        () async {
      // premo il pulsante di iscriviti senza inserire tutti i campi
      //mi aspetto di rimanere nella pagina di registrazione

      //----
      // inserisco tutto tranne la mail
      final passwordField = find.byValueKey(Keys.passwordField);
      await insertText(passwordField, userPassword);

      final confermaPasswordField = find.byValueKey(Keys.confermaPasswordField);
      await insertText(confermaPasswordField, userPassword);

      final nomeField = find.byValueKey(Keys.nomeField);
      await insertText(nomeField, userName);

      final cognomeField = find.byValueKey(Keys.cognomeField);
      await insertText(cognomeField, userSurname);

      // cerca il bottone di iscrizione
      final iscrivitiButton = find.byValueKey(Keys.iscrivitiButton);
      await pressButton(iscrivitiButton, false);

      //----
      //inserisco tutto tranne password
      final emailField = find.byValueKey(Keys.emailField);
      await insertText(emailField, userMail);
      await insertText(passwordField, '');

      await pressButton(iscrivitiButton, false);

      //----
      //inserisco tutto tranne conferma password
      await insertText(confermaPasswordField, '');
      await insertText(passwordField, userPassword);

      await pressButton(iscrivitiButton, false);

      //----
      //inserisco tutto tranne nome
      await insertText(nomeField, '');
      await insertText(confermaPasswordField, userPassword);

      await pressButton(iscrivitiButton, false);

      //----
      //inserisco tutto tranne cognome
      await insertText(cognomeField, '');
      await insertText(nomeField, userName);

      await pressButton(iscrivitiButton, false);
    });
    test('isciviti come venditore inserendo tutti tranne un campo richiesto',
        () async {
      // premo il pulsante di iscriviti senza inserire tutti i campi
      //mi aspetto di rimanere nella pagina di registrazione

      //----
      // inserisco tutto tranne la mail e aggiungo tutte le info del venditore
      final emailField = find.byValueKey(Keys.emailField);
      await insertText(emailField, '');

      final passwordField = find.byValueKey(Keys.passwordField);
      await insertText(passwordField, userPassword);

      final confermaPasswordField = find.byValueKey(Keys.confermaPasswordField);
      await insertText(confermaPasswordField, userPassword);

      final nomeField = find.byValueKey(Keys.nomeField);
      await insertText(nomeField, userName);

      final cognomeField = find.byValueKey(Keys.cognomeField);
      await insertText(cognomeField, userSurname);

      //seleziono sono un venditore
      final sonoVenditoreFlag = find.byValueKey(Keys.sonoVenditoreFlag);
      await premiFlag(sonoVenditoreFlag);

      final denominazioneSocialeField =
          find.byValueKey(Keys.denominazioneSocialeField);
      await insertText(denominazioneSocialeField, companyName);

      final indirizzoField = find.byValueKey(Keys.indirizzoField);
      await insertText(indirizzoField, address);

      final nomeAttivitaField = find.byValueKey(Keys.nomeAttivitaField);
      await insertText(nomeAttivitaField, businnessName);

      final partitaIvaField = find.byValueKey(Keys.partitaIvaField);
      await insertText(partitaIvaField, vatNumber);

      // cerca il bottone di iscrizione
      final iscrivitiButton = find.byValueKey(Keys.iscrivitiButton);
      await pressButton(iscrivitiButton, false);

      //----
      //inserisco tutto tranne password
      await insertText(emailField, userMail);
      await insertText(passwordField, '');

      await pressButton(iscrivitiButton, false);

      //----
      //inserisco tutto tranne conferma password
      await insertText(confermaPasswordField, '');
      await insertText(passwordField, userPassword);

      await pressButton(iscrivitiButton, false);

      //----
      //inserisco tutto tranne nome
      await insertText(nomeField, '');
      await insertText(confermaPasswordField, userPassword);

      await pressButton(iscrivitiButton, false);

      //----
      //inserisco tutto tranne cognome
      await insertText(cognomeField, '');
      await insertText(nomeField, userName);

      await pressButton(iscrivitiButton, false);

      //----
      //inserisco tutto tranne denominazione sociale
      await insertText(denominazioneSocialeField, '');
      await insertText(cognomeField, userSurname);

      await pressButton(iscrivitiButton, false);

      //----
      //inserisco tutto tranne indirizzo
      await insertText(indirizzoField, '');
      await insertText(denominazioneSocialeField, companyName);

      await pressButton(iscrivitiButton, false);

      //----
      //inserisco tutto tranne nome attivita
      await insertText(nomeAttivitaField, '');
      await insertText(indirizzoField, address);

      await pressButton(iscrivitiButton, false);

      //----
      //inserisco tutto tranne partita iva
      await insertText(partitaIvaField, '');
      await insertText(nomeAttivitaField, businnessName);

      await pressButton(iscrivitiButton, false);
    });

    test('isciviti inserendo una mail in formato errato', () async {
      // premo il pulsante di iscriviti inserendo campi non validi
      //mi aspetto di rimanere nella pagina di registrazione

      //----
      // inserisco tutto con la mail non valida
      final emailField = find.byValueKey(Keys.emailField);
      await insertText(emailField, 'mail');

      final passwordField = find.byValueKey(Keys.passwordField);
      await insertText(passwordField, userPassword);

      final confermaPasswordField = find.byValueKey(Keys.confermaPasswordField);
      await insertText(confermaPasswordField, userPassword);

      final nomeField = find.byValueKey(Keys.nomeField);
      await insertText(nomeField, userName);

      final cognomeField = find.byValueKey(Keys.cognomeField);
      await insertText(cognomeField, userSurname);

      final denominazioneSocialeField =
          find.byValueKey(Keys.denominazioneSocialeField);
      await insertText(denominazioneSocialeField, companyName);

      final indirizzoField = find.byValueKey(Keys.indirizzoField);
      await insertText(indirizzoField, address);

      final nomeAttivitaField = find.byValueKey(Keys.nomeAttivitaField);
      await insertText(nomeAttivitaField, businnessName);

      final partitaIvaField = find.byValueKey(Keys.partitaIvaField);
      await insertText(partitaIvaField, vatNumber);

      // cerca il bottone di iscrizione
      final iscrivitiButton = find.byValueKey(Keys.iscrivitiButton);
      await pressButton(iscrivitiButton, false);
    });

    test('isciviti inserendo mail gia usata', () async {
      // premo il pulsante di iscriviti inserendo campi non validi
      //mi aspetto di rimanere nella pagina di registrazione

      // inserisco una mail già presente nel db
      final emailField = find.byValueKey(Keys.emailField);
      await insertText(emailField, 'amministratore@raff.it');

      final passwordField = find.byValueKey(Keys.passwordField);
      await insertText(passwordField, userPassword);

      final confermaPasswordField = find.byValueKey(Keys.confermaPasswordField);
      await insertText(confermaPasswordField, userPassword);

      final nomeField = find.byValueKey(Keys.nomeField);
      await insertText(nomeField, userName);

      final cognomeField = find.byValueKey(Keys.cognomeField);
      await insertText(cognomeField, userSurname);

      final denominazioneSocialeField =
          find.byValueKey(Keys.denominazioneSocialeField);
      await insertText(denominazioneSocialeField, companyName);

      final indirizzoField = find.byValueKey(Keys.indirizzoField);
      await insertText(indirizzoField, address);

      final nomeAttivitaField = find.byValueKey(Keys.nomeAttivitaField);
      await insertText(nomeAttivitaField, businnessName);

      final partitaIvaField = find.byValueKey(Keys.partitaIvaField);
      await insertText(partitaIvaField, vatNumber);

      // cerca il bottone di iscrizione
      final iscrivitiButton = find.byValueKey(Keys.iscrivitiButton);
      await pressButton(iscrivitiButton, false);
    });

    test('isciviti inserendo una password non valida', () async {
      // premo il pulsante di iscriviti inserendo campi non validi
      //mi aspetto di rimanere nella pagina di registrazione

      //inserisco una password non valida (lunghezza < 6 caratteri)
      final emailField = find.byValueKey(Keys.emailField);
      await insertText(emailField, userMail);

      final passwordField = find.byValueKey(Keys.passwordField);
      await insertText(passwordField, 'aaaaa');

      final confermaPasswordField = find.byValueKey(Keys.confermaPasswordField);
      await insertText(confermaPasswordField, 'aaaaa');

      final nomeField = find.byValueKey(Keys.nomeField);
      await insertText(nomeField, userName);

      final cognomeField = find.byValueKey(Keys.cognomeField);
      await insertText(cognomeField, userSurname);

      final denominazioneSocialeField =
          find.byValueKey(Keys.denominazioneSocialeField);
      await insertText(denominazioneSocialeField, companyName);

      final indirizzoField = find.byValueKey(Keys.indirizzoField);
      await insertText(indirizzoField, address);

      final nomeAttivitaField = find.byValueKey(Keys.nomeAttivitaField);
      await insertText(nomeAttivitaField, businnessName);

      final partitaIvaField = find.byValueKey(Keys.partitaIvaField);
      await insertText(partitaIvaField, vatNumber);

      // cerca il bottone di iscrizione
      final iscrivitiButton = find.byValueKey(Keys.iscrivitiButton);
      await pressButton(iscrivitiButton, false);

      //----
      //inserisco la conferma password diversa dalla password inserita
      await insertText(passwordField, userPassword);
      await insertText(confermaPasswordField, 'bbbbbb');

      await pressButton(iscrivitiButton, false);
    });
    test('isciviti come venditore', () async {
      // premo il pulsante di iscriviti dopo aver inserito i campi richiesti
      //mi aspetto di registrarmi nella pagina di registrazione

      //----
      // compilo tutti i campi correttamente
      final emailField = find.byValueKey(Keys.emailField);
      await insertText(emailField, userMail);

      final passwordField = find.byValueKey(Keys.passwordField);
      await insertText(passwordField, userPassword);

      final confermaPasswordField = find.byValueKey(Keys.confermaPasswordField);
      await insertText(confermaPasswordField, userPassword);

      final nomeField = find.byValueKey(Keys.nomeField);
      await insertText(nomeField, userName);

      final cognomeField = find.byValueKey(Keys.cognomeField);
      await insertText(cognomeField, userSurname);

      // //seleziono sono un venditore
      // final sonoVenditoreFlag = find.byValueKey(Keys.sonoVenditoreFlag);
      // await premiFlag(sonoVenditoreFlag);

      final denominazioneSocialeField =
          find.byValueKey(Keys.denominazioneSocialeField);
      await insertText(denominazioneSocialeField, companyName);

      final indirizzoField = find.byValueKey(Keys.indirizzoField);
      await insertText(indirizzoField, address);

      final nomeAttivitaField = find.byValueKey(Keys.nomeAttivitaField);
      await insertText(nomeAttivitaField, businnessName);

      final partitaIvaField = find.byValueKey(Keys.partitaIvaField);
      await insertText(partitaIvaField, vatNumber);

      // cerca il bottone di iscrizione
      final iscrivitiButton = find.byValueKey(Keys.iscrivitiButton);
      await pressButton(iscrivitiButton, true);

      //elinino l'account per provare nuovi test
      eliminaAccount(true);
    });
    test('isciviti come compratore', () async {
      // premo il pulsante di iscriviti dopo aver inserito i campi richiesti
      //mi aspetto di registrarmi nella pagina di registrazione

      // passo alla pagina di registrazione
      final toIscrivitiPage = find.byValueKey(Keys.toIscrivitiPage);
      await driver.waitFor(toIscrivitiPage);
      await driver.tap(toIscrivitiPage);

      // compilo tutti i campi correttamente
      final emailField = find.byValueKey(Keys.emailField);
      await insertText(emailField, userMail);

      final passwordField = find.byValueKey(Keys.passwordField);
      await insertText(passwordField, userPassword);

      final confermaPasswordField = find.byValueKey(Keys.confermaPasswordField);
      await insertText(confermaPasswordField, userPassword);

      final nomeField = find.byValueKey(Keys.nomeField);
      await insertText(nomeField, userName);

      final cognomeField = find.byValueKey(Keys.cognomeField);
      await insertText(cognomeField, userSurname);

      //accetto le condizioni
      final accettoCondizioniFlag = find.byValueKey(Keys.accettoCondizioniFlag);
      await premiFlag(accettoCondizioniFlag);

      // cerca il bottone di iscrizione
      final iscrivitiButton = find.byValueKey(Keys.iscrivitiButton);
      await pressButton(iscrivitiButton, true);
      //alla fine del test mi ritrovo nella pagina della home dell'account email@email.com
    });
  }, timeout: Timeout(Duration(minutes: 2)));
}
