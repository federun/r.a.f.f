
# Descrizione
Questo repository contiene il codice flutter/dart riguardante un applicazione di ecommerce (*R.A.F.F*). 

# Organizzazione file e cartelle
I file all'interno del repository seguono la struttura tipica di un progetto flutter. In particolare la codebase è situata all'interno della cartella **lib**, oganizzata come segue:
* models: contiene le classi modello, ovvero quelle classi che rappresenteranno gli oggetti (utente, venditore etc...) nell'applicazione
* screens: contiene gli elementi visivi, le pagine e i widget utilizzati per la creazione della parte grafica dell'applicazione. Questi sono organizzati secondo la strutta visuale dell'applicazione.
* services: contiene le classi che si occupano di eseguire delle azioni (effettuare accesso, disconnessione, aggiunta prodotti etc...)
* shared: contiene le parti di codice comune a tutti gli screens, ovvero quelle parti che sono da riutilizzare all'interno della grafica, come le barre superiori ed il drawer.


# Documentazione storie:
Ogni storia è qui sotto documentata, ed è descritta per mezzo di quello che fa, di come lo ha realizzato, e di come è stata testata.




## Registrazione
*screens/authenticate/register.dart* è la pagina che consente all'utente di registrarsi all'app. Vi dovrà inserire informazioni quali email, password, nome e cognome. Nel caso nel quale a registrarsi sia un venditore, dopo aver spuntato la casella "sono un venditore" esso dovrà inserire alcune informazioni della sua attività.
Se le credenziali inserite sono corrette (tutti i campi compilati e password di almeno 6 caratteri), la pressione del pulsate "continua" effettua una richiesta di creazione utente ad auth di firestore. Se la creazione ha successo (mail non già utilizzata e con formato corretto) si esce dalla modalità autenticazione cambiando stato di showSignIn in *screen/authenticate/authenticate.dart* e viene mostrata la home page.
Email e password vengono salvate attraverso la funzione *registerWithEmailAndPassword* in *services/auth.dart* utilizzando FirebaseAuth, che genera un codice univoco che rimarrà invariato per tutta la durata di vita di tale account. Questo codice verrà utilizzato per identificare tutte le attività che verranno effettuate da tale account.
Il nome e cognome e le eventuali informazioni da venditore vengono salvati nel documento "utente" di firestore, con chiave l'id dell'utente.

#### test
Per eseguire i test digitare da riga di comando flutter drive --target=test_driver/app.dart --driver=test_driver/authenticate/register_test.dart
I test comprendono la tentata registrazione senza successo per non aver inserito tutti o alcuni i campi richiesti, non aver rispettato i formati richiesti di mail e password e la richiesta di iscriversi con una mail già presente nel database. Infine sono presenti due registrazioni con successo rispetivamente per un cliente ed un venditore.

## Accesso
*screens/authenticate/sign_in.dart* è la pagina che consente all'utente già registrato di accedere all'app. Verrà richiesta solo l'email e la password.
Viene effettuata una richiesta a FirebaseAuth attraverso la funzione *signInWithEmailAndPassword* in *services/auth.dart*, se trova un utente registrato con le credenziali passate ritorna il suo id ed esce dalla modalità di autenticazione, altrimenti restituisce null e viene richiesto di reinserire le credenziali. Questa schermata è la prima che viene mostrata all'accensione dell'app. Per i nuovi utenti è presente un pulsante per spostarsi nella pagina di registrazione.
 
#### test
Per eseguire i test digitare da riga di comando flutter drive --target=test_driver/app.dart --driver=test_driver/authenticate/sign_in_test.dart
I test comprendono il tentato accesso all'app utilizzando credenziali errate o incomplete e un accesso concluso correttamente

## Recupero password
nella pagina di sign in *screens/authenticate/sign_in.dart* è stato aggiunto un pulsante, che dopo essere premuto chiama la funzione *resetPassword* in *services/auth.dart* che invia una mail all'indirizzo indicato nel campo della email in sign in. La email inviata conterrà un link che porta ad una pagina web dove viene richiesto di inserire la nuova password. La password inserita andrà a sovrascrivere la precedente.

#### test
I test verificano l'invio o no della mail all'indirizzo fornito. Sono stati aggiunti insieme a quelli del sign-in



## Aggiunta prodotto
Nella pagina **il mio negozio** è presente una griglia con tutte le possibili operazioni. Un utente negoziante ha la possibilità di accere ad un negozio e visualizzare queste informazioni.
L'aggiunta di un prodotto viene fatta prendendo l'id del venditore, che è lo stesso di un normale utente. Il prodotto sarà inserito in una collezione e al suo interno sarà definito l'id del venditore oltre ad una serie di informazioni: nome, descrizione, tags, prezzo, quantità e immagini.

Le operazioni di inserimento sono gestite in *services/seller.dart*. Prima sono aggiunte le immagini e poi i relativi url sono aggiunti alle informazioni testuali nella collezione Prodotti.
E' presente anche un metodo utilizzato per trasformare il documento remoto in un oggetto Venditore (*seller*) locale, in modo che le logiche di firebase e quelle locali siano separate.
Gli elementi visivi sono stati separati in modo da creare un Widget apposito per gestire le immagini.

#### test
I test utilizzati sono quelli di integrazione, i più completi delle tre tipologie che flutter mette a disposizione. Questi test non sono però corretti al 100%, il driver non è in grado ad ora di accedere alla fotocamera o alla galleria, in quando esterni al contesto di esecuzione dello stesso.



## Dettaglio Prodotto
Quando viene selezionato un prodotto è necessario poter visualizzare le informazioni specifiche su questo. Ciò è realizzato per mezzo di un file che consente di mostrare le informazioni sul prodotto, di visualizzare le immagini e consentire all'utente di aggiungere al carrello il prodotto, mostrando le quantità ad ora presenti.
La realizzazione è stata fatta sfruttando i services **services/CartService** e ottenendo il prodotto attraverso il costruttore della pagina. All'interno di essa è presente un widget per poter visualizzare le immagini associate al prodotto, che sono scaricate e messe in cache man mano che l'utente le scorre.
Il feedback visivo sul numero di prodotti è realizzato per mezzo di uno stream, che garantisce un aggiornamento in tempo reale della quantità presente nel carrello.

#### test
I test sono stati svolti utilizzando il driver e testando che il sistema aggiungesse effettivamente la quantità di prodotto al carrello sfruttando il valore ottenuto dallo streambuilder.


## Carrello
Nella pagina carrello sono visualizzati i prodotti che l'utente ha inserito nel carrello, la quantità, il prezzo del singolo prodotto e il prezzo totale.  E' inoltre possibile modificare la quantità desiderata per ogni prodotto o rimuovere il prodotto dal carrello (il prodotto viene rimosso automaticamente se la quantità è uguale a 0).

Le operazioni di visualizzazione, e modifica della quantità sono gestite in *services/CartServices.dart*:
* *getCart(String uid)*: ritorna un oggetto di tipo Cart (definito in *mdels/Cart.dart*) contenente i prodotti che l'utnente (id: *uid*) ha nel carrello e la relativa quantità;
* *addToCart(String uid, String productId, {int addQuantity = 1})*: aggiunge un prodotto (id: *productId*) al carrello dell'udente (id: *uid*), nella quantità *addQuantity*. Se il prodotto non è presente nel carrello lo aggiunge, se è presente ne aumenta la quantità (verificando e aggiornando la disponibilità);
* *removeToCart(String uid, String productId,  {int removeQuantity = 1})*: rimuove un prodotto (id: *productId*) dal carrello dell'udente (id: *uid*), nella quantità *removeQuantity*. Una volta rimosso, viene aggiornata la disponibilià del prodotto e se la quantità nel carrello è pari a 0, il prodotto viene rimosso dal carrello;


#### test
Le funzioni sono state testate attraverso test di integrazione che comprendono le fasi di: login, accesso alla sezione carrello, aggiunta e rimozione di prodotti dal carrello. Tutti i test sono stati superati con successo.

