class Product {
  final String id, description, name, sellerId;
  final num price, quantity;
  final List<dynamic> images, tags;
  Product(
      {this.id,
      this.description,
      this.name,
      this.sellerId,
      this.price,
      this.quantity,
      this.images,
      this.tags});
}
