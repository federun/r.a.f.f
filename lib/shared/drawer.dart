import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:raaf_project/models/UserApp.dart';
import 'package:raaf_project/screens/home/home.dart';
import 'package:raaf_project/screens/my_purchases/my_purchases.dart';
import 'package:raaf_project/screens/my_shop/my_shop.dart';
import 'package:raaf_project/screens/shopping_cart/shopping_cart.dart';
import 'package:raaf_project/screens/wrapper.dart';
import 'package:raaf_project/services/auth.dart';
import 'package:raaf_project/shared/keys.dart';
import 'package:raaf_project/shared/loading.dart';

class RaffDrawer extends StatefulWidget {
  RaffDrawer();

  @override
  _RaffDrawerState createState() => _RaffDrawerState();
}

class _RaffDrawerState extends State<RaffDrawer> {
  final AuthService _auth = AuthService();

  bool loading;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserApp>(context);

    return user == null
        ? Loading()
        : Drawer(
            // Add a ListView to the drawer. This ensures the user can scroll
            // through the options in the drawer if there isn't enough vertical
            // space to fit everything.
            child: ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: <Widget>[
                DrawerHeader(
                  child: Text('Drawer Header'),
                  decoration: BoxDecoration(
                    color: Colors.blue,
                  ),
                ),
                ListTile(
                  title: Text(user.nome + ' ' + user.cognome + ' ' + user.uid),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: Text(user.email),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: Text("Carrello"),
                  key: Key(Keys.shoppingCart),
                  onTap: () {
                    Navigator.pushNamed(context, ShoppingCart.routeName,
                        arguments: user);
                  },
                ),
                ListTile(
                  title: Text("I miei acquisti"),
                  onTap: () {
                    Navigator.pushNamed(context, MyPurchases.routeName,
                        arguments: user);
                  },
                ),
                ListTile(
                  key: Key(Keys.myShopDrawer),
                  title: Text("Il mio Negozio"),
                  onTap: () {
                    Navigator.pushNamed(context, MyShop.routeName,
                        arguments: user);
                  },
                ),
                ListTile(
                  title: Text("Home"),
                  onTap: () {
                    Navigator.pushNamed(context, Home.routeName,
                        arguments: user);
                  },
                ),
                TextButton.icon(
                  key: Key(Keys.logoutButton),
                  icon: Icon(Icons.person),
                  label: Text("Logout"),
                  onPressed: () async {
                    setState(() => loading = true);

                    Navigator.of(context).pushNamedAndRemoveUntil(
                        Wrapper.routeName, (Route<dynamic> route) => false);
                    await _auth.signOut();
                    setState(() => loading = false);
                  },
                ),
              ],
            ),
          );
  }
}
