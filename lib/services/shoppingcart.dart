import 'package:raaf_project/models/Product.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

// better than map
class ProductQuantityPair<Product, num> {
  final Product product;
  final num quantity;
  ProductQuantityPair(this.product, this.quantity);
}

class CartService {
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('utente');
  final CollectionReference productCollection =
      FirebaseFirestore.instance.collection('prodotto');

  Stream<DocumentSnapshot> totalInCart(String uid, String productId) {
    return userCollection.doc(uid).snapshots();
  }

  Future<Product> getProduct(String productId) async {
    try {
      dynamic product = await productCollection.doc(productId).get();
      String name = product.get("nome");
      String description = product.get("descrizione");
      String sellerId = product.get("venditore");
      num price = product.get("prezzo");
      num quantity = product.get("quantita");
      List<dynamic> images = product.get("immagini");
      List<dynamic> tags = product.get("tags");
      return Product(
          id: productId,
          name: name,
          description: description,
          sellerId: sellerId,
          price: price,
          quantity: quantity,
          images: images,
          tags: tags);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<List<ProductQuantityPair<Product, num>>> _cartFromFirebase(
      dynamic cart, String uid) async {
    Map<String, dynamic> dummyCart = await cart.get("prodottiCarrello");

    List<ProductQuantityPair> products = <ProductQuantityPair<Product, num>>[];

    for (var tmp in dummyCart.entries) {
      await getProduct(tmp.key).then((value) {
        if (value != null) {
          ProductQuantityPair<Product, num> tmpPair =
              ProductQuantityPair(value, tmp.value);
          products.add(tmpPair);
        } else {
          // product does not exist anymore in product collection. have to remove from cart
          userCollection
              .doc(uid)
              .update({"prodottiCarrello.${tmp.key}": FieldValue.delete()});
        }
      });
    }

    return products;
  }

  Stream<Future<List<ProductQuantityPair<Product, num>>>> getCart(
      {String uid}) {
    return userCollection.doc(uid).snapshots().map((event) {
      return _cartFromFirebase(event, uid);
    });
  }

  _productFromFireBase(DocumentSnapshot firebaseProduct, String id) {
    return firebaseProduct != null
        ? Product(
            id: id,
            description: firebaseProduct.get("descrizione"),
            name: firebaseProduct.get("nome"),
            sellerId: firebaseProduct.get("venditore"),
            price: firebaseProduct.get("prezzo"),
            quantity: firebaseProduct.get("quantita"),
            images: firebaseProduct.get("immagini"),
            tags: firebaseProduct.get("tags"))
        : null;
  }

  Stream<Product> getProductStream(String productId) {
    return productCollection
        .doc(productId)
        .snapshots()
        .map((product) => _productFromFireBase(product, productId));
  }

  Future<void> addToCart(String uid, String productId,
      {int addQuantity = 1}) async {
    dynamic product = productCollection.doc(productId);
    bool added = false;

    await FirebaseFirestore.instance.runTransaction((transaction) async {
      DocumentSnapshot document = await transaction.get(product);
      dynamic quantity = document.get("quantita");

      if (quantity > 0 && quantity >= addQuantity) {
        transaction.update(product, {'quantita': quantity - addQuantity});
        added = true;
      }
    }).catchError((onError) {
      added = false;
      print("Transaction failed: $onError");
    });

    if (added) {
      dynamic cart = await userCollection.doc(uid).get();
      Map cartProducts = cart.get("prodottiCarrello");
      if (cartProducts.containsKey(productId)) {
        dynamic quantity = cartProducts[productId];
        userCollection
            .doc(uid)
            .update({"prodottiCarrello.$productId": addQuantity + quantity});
      } else {
        userCollection
            .doc(uid)
            .update({"prodottiCarrello.$productId": addQuantity});
      }
    }
  }

  Future<void> removeToCart(String uid, String productId,
      {int removeQuantity = 1}) async {
    dynamic product = productCollection.doc(productId);
    bool removed = false;

    await FirebaseFirestore.instance.runTransaction((transaction) async {
      DocumentSnapshot document = await transaction.get(product);
      dynamic quantity = document.get("quantita");
      transaction.update(product, {'quantita': quantity + removeQuantity});
      removed = true;
    }).catchError((onError) {
      removed = false;
      print("Transaction failed: $onError");
    });

    if (removed) {
      dynamic cart = await userCollection.doc(uid).get();
      Map cartProducts = cart.get("prodottiCarrello");
      int quantity = cartProducts[productId] - removeQuantity;
      if (quantity < 1)
        await userCollection
            .doc(uid)
            .update({"prodottiCarrello.$productId": FieldValue.delete()});
      else
        await userCollection
            .doc(uid)
            .update({"prodottiCarrello.$productId": quantity});
    }
  }
}
