// project-specific imports
import 'dart:io';
import 'dart:typed_data';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/services.dart';
import 'package:raaf_project/main.dart';
//import 'package:raaf_project/services/auth_service_adapter.dart';
// flutter-specific imports
import 'package:flutter/material.dart';
import 'package:flutter_driver/driver_extension.dart';

void main() async {
  enableFlutterDriverExtension();
  WidgetsFlutterBinding.ensureInitialized();
  // This line enables the extension.
  await Firebase.initializeApp();
  // Call the `main()` function of the app, or call `runApp` with
  // any widget you are interested in testing.
  //runApp(MyApp(initialAuthServiceType: AuthServiceType.mock));

  const MethodChannel channel =
      MethodChannel('plugins.flutter.io/image_picker');

  channel.setMockMethodCallHandler((MethodCall methodCall) async {
    ByteData data = await rootBundle.load('images/tests/image-test-red.jpg');

    Uint8List bytes = data.buffer.asUint8List();
    Directory tempDir = Directory.systemTemp;

    File file = await File(
      '${tempDir.path}/tmp.tmp',
    ).writeAsBytes(bytes);
    print(file.path);
    return file.path;
  });

  runApp(MyApp());
}
