class Seller {
  final String uid; // same as UserApp
  final String vatNumber;
  final String activityName;
  final String companyName;
  final String address;

  Seller(
      {this.uid,
      this.vatNumber,
      this.activityName,
      this.companyName,
      this.address});
}
