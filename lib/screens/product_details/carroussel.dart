import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dots_indicator/dots_indicator.dart';

class Carroussel extends StatefulWidget {
  final List<String> images;

  Carroussel({this.images});

  @override
  _CarrousselState createState() => new _CarrousselState();
}

class _CarrousselState extends State<Carroussel> {
  int _position = 0;
  bool _showDots = false;
  PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(
      viewportFraction: 1,
    );
  }

  @override
  Widget build(BuildContext context) {
    _showDots = widget.images.length > 1;

    return Column(
      children: [
        SizedBox(
          height: 250,
          child: PageView.builder(
            physics: new AlwaysScrollableScrollPhysics(),
            itemCount: widget.images.length,
            controller: _pageController,
            onPageChanged: (value) {
              setState(() {
                _position = value;
              });
            },
            itemBuilder: (context, index) {
              return imageSlider(index);
            },
          ),
        ),
        Container(
          padding: EdgeInsets.only(left: 20, top: 20),
          alignment: Alignment.topLeft,
          child: _showDots
              ? SizedBox(
                  height: 25,
                  child: DotsIndicator(
                    dotsCount: widget.images.length,
                    position: _position.toDouble(),
                    decorator: DotsDecorator(
                        color: Colors.grey.shade300, // Inactive color
                        activeColor: Colors.blue.shade500,
                        activeSize: Size(15, 15)),
                  ))
              : SizedBox(
                  height: 25,
                ),
        ),
      ],
    );
  }

  imageSlider(int index) {
    return AnimatedBuilder(
      animation: _pageController,
      child: Container(
        margin: EdgeInsets.only(left: 10, right: 10),
        child: CachedNetworkImage(
          placeholder: (context, url) =>
              Center(child: CircularProgressIndicator()),
          imageUrl: widget.images[index],
          imageBuilder: (context, imageProvider) => Container(
              decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.fill,
            ),
          )),
        ),
      ),
      builder: (context, widget) {
        double value = 1;
        if (_pageController.position.haveDimensions) {
          value = _pageController.page - index;
          value = (1 - (value.abs() * 0.7)).clamp(0.0, 1.0);
        }

        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(value) * 250,
            child: widget,
          ),
        );
      },
    );
  }
}
