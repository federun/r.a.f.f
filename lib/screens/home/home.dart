import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:raaf_project/models/UserApp.dart';
import 'package:raaf_project/models/Product.dart';
import 'package:raaf_project/screens/product_details/product_datails.dart';
import 'package:raaf_project/services/auth.dart';
import 'package:raaf_project/shared/app_bar.dart';
import 'package:raaf_project/shared/drawer.dart';
import 'package:raaf_project/shared/keys.dart';

class Home extends StatefulWidget {
  static String routeName = "/Home";
  final UserApp user;

  Home({this.user});

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final AuthService auth = AuthService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: RaffAppBar(
        pageKey: Keys.homePage,
        appBarHeight: 60.0,
        appBarColor: Colors.blue,
      ),
      drawer: RaffDrawer(),
      body: Center(
        child: TextButton(
          key: Key(Keys.temporaryButtonForSingleProduct),
          child: Text("Pulsante temporaneo per il prodotto"),
          onPressed: () {
            Navigator.pushNamed(
              context,
              ProductDetails.routeName,
              arguments: <Object>[
                widget.user,
                Product(
                    id: "aNhGTmhfmQEr1YKeAEck",
                    description:
                        "Descrizione molto lunga riguardo un ombrello nero, che non è un vestito, ma volevo fare la foto all'ombrello. Non ho voglia di cercare altri vestiti dato che nella galleria del telefono non ne ho.",
                    name: "Ombrello nero compatto",
                    sellerId: "5QYV1mKeDtZZecDdGKsYxDa5xqf2",
                    price: 10.2,
                    quantity: 18,
                    images: <String>[
                      "https://firebasestorage.googleapis.com/v0/b/raaf-93ed9.appspot.com/o/ImmaginiProdotti%2F5QYV1mKeDtZZecDdGKsYxDa5xqf2__Ombrellonerocompatto__20210408231913843582__0?alt=media&token=5e179c19-0105-442a-8bf6-b09e9123b9a4",
                      "https://firebasestorage.googleapis.com/v0/b/raaf-93ed9.appspot.com/o/ImmaginiProdotti%2F5QYV1mKeDtZZecDdGKsYxDa5xqf2__Ombrellonerocompatto__20210408231913843582__1?alt=media&token=b2fad1e9-5f60-4d7b-ae33-972f9b4be5d7",
                      "https://firebasestorage.googleapis.com/v0/b/raaf-93ed9.appspot.com/o/ImmaginiProdotti%2F5QYV1mKeDtZZecDdGKsYxDa5xqf2__Ombrellonerocompatto__20210408231913843582__2?alt=media&token=88311a6d-6552-458a-9c48-58a75fd98084",
                      "https://firebasestorage.googleapis.com/v0/b/raaf-93ed9.appspot.com/o/ImmaginiProdotti%2F5QYV1mKeDtZZecDdGKsYxDa5xqf2__Ombrellonerocompatto__20210408231913843582__3?alt=media&token=cea0920a-7981-43dd-acec-4718d0547788"
                    ],
                    tags: <String>[
                      "vendo scarpe rotte",
                      "ombrello",
                      "ombrellonero",
                      "pioggia"
                    ])
              ],
            );
          },
        ),
      ),
      /* end temporary code*/
    );
  }
}
