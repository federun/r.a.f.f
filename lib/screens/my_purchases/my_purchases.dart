import 'package:flutter/material.dart';
import 'package:raaf_project/models/UserApp.dart';
import 'package:raaf_project/shared/drawer.dart';

class MyPurchases extends StatefulWidget {
  static String routeName = "/MyPurchases";

  final UserApp user;
  MyPurchases({this.user});

  @override
  _MyPurchasesState createState() => _MyPurchasesState();
}

class _MyPurchasesState extends State<MyPurchases> {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 60,
        backgroundColor: Colors.blue,
        elevation: 0.0,
        title: Text("I miei Acquisti"),
      ),
      drawer: RaffDrawer(),
      resizeToAvoidBottomInset: false,
      body: Center(child: Text("MyPurchases here")),
    );
  }
}
