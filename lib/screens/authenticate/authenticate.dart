import 'package:flutter/material.dart';
import 'package:raaf_project/screens/authenticate/register.dart';
import 'package:raaf_project/screens/authenticate/sign_in.dart';

class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {
  bool _showSingIn = true;

  void toogleView() {
    setState(() {
      _showSingIn = !_showSingIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (_showSingIn) {
      return Container(child: SignIn(toggleView: toogleView));
    } else
      return Container(child: Register(toggleView: toogleView));
  }
}
