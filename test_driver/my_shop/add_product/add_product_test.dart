import 'package:flutter_driver/flutter_driver.dart';
import 'package:raaf_project/shared/keys.dart';
import 'package:test/test.dart';

void main() {
  FlutterDriver driver;

  Future<void> delay([int milliseconds = 250]) async {
    await Future<void>.delayed(Duration(milliseconds: milliseconds));
  }

  // Connect to the Flutter driver before running any tests.
  setUpAll(() async {
    driver = await FlutterDriver.connect();
  });

  tearDownAll(() async {
    if (driver != null) {
      driver.close();
    }
  });

  test('check flutter driver health', () async {
    final health = await driver.checkHealth();
    expect(health.status, HealthStatus.ok);
  });

  test('accedi con credenziali corrette', () async {
    // premo il pulsante di accedi inserendo la password e la mail corretta
    //mi aspetto di arrivare alla home page

    // inserimento email
    final emailField = find.byValueKey(Keys.emailField);
    await driver.waitFor(emailField);
    await delay(750); // for video capture
    await driver.tap(emailField);
    await driver.enterText('l.mendozzi@campus.unimib.it');
    await driver.waitFor(find.text('l.mendozzi@campus.unimib.it'));

    // inserimento password
    final passwordField = find.byValueKey(Keys.passwordField);
    await driver.waitFor(passwordField);
    await delay(750); // for video capture
    await driver.tap(passwordField);
    await driver.enterText('qwerty123456');
    await driver.waitFor(find.text('qwerty123456'));

    // cerca il bottone di accesso e lo premo
    final signInButton = find.byValueKey(Keys.accediButton);
    await driver.waitFor(signInButton);
    await delay(750); // for video capture
    await driver.tap(signInButton);

    // aspetto 2 secondi e controllo se mi trovo nella home page
    await delay(2000);
    final homePage = find.byValueKey(Keys.homePage);
    await driver.waitFor(homePage);
  });

  test('Open drawer and go to my shop page', () async {
    final drawer = find.byTooltip('Open navigation menu');
    await driver.waitFor(drawer);
    await driver.tap(drawer);
    await delay(1000);
    final myShop = find.byValueKey(Keys.myShopDrawer);
    await driver.waitFor(myShop);
    await delay(300);
    driver.tap(myShop);
    await delay(5000);
    final myShopPage = find.byValueKey(Keys.pageMyShop);
    await driver.waitFor(myShopPage);
  });

  test('Open add product page', () async {
    final addNewProduct = find.byValueKey(Keys.buttonAddProduct);
    await driver.waitFor(addNewProduct);
    await driver.tap(addNewProduct);
    await delay(1000);

    final pageMyProduct = find.byValueKey(Keys.pageAddProduct);
    await driver.waitFor(pageMyProduct);
    await delay(1000);
  });

  test('Aggiungi prodotto vuoto', () async {
    final productScroll = find.byValueKey(Keys.productScroll);
    final buttonAddProductToMyShop =
        find.byValueKey(Keys.buttonAddProductToMyShop);
    final productName = find.byValueKey(Keys.productNameField);

    await driver.scrollUntilVisible(productScroll, buttonAddProductToMyShop,
        dyScroll: -100.0);

    await driver.tap(buttonAddProductToMyShop);
    await delay(1000);

    await driver.scrollUntilVisible(productScroll, productName,
        dyScroll: 100.0);

    await delay(3000);
  });

  test('Aggiungi prodotto solo testo', () async {
    final productScroll = find.byValueKey(Keys.productScroll);
    final buttonAddProductToMyShop =
        find.byValueKey(Keys.buttonAddProductToMyShop);

    final productName = find.byValueKey(Keys.productNameField);
    await driver.waitFor(productName);
    await delay(750); // for video capture
    await driver.tap(productName);
    await driver.enterText('prodotto di prova');
    await driver.waitFor(find.text('prodotto di prova'));

    final tags = find.byValueKey(Keys.producTagField);
    await driver.waitFor(tags);
    await delay(750); // for video capture
    await driver.tap(tags);

    await driver.enterText(' alcuni ');
    await delay(750); // for video capture
    await driver.enterText(' tag ');
    await delay(4000);

    final description = find.byValueKey(Keys.productDescriptionField);
    await driver.waitFor(description);
    await delay(750); // for video capture
    await driver.tap(description);
    await driver.enterText('descrizione del prodotto di prova, nulla di più');
    await driver
        .waitFor(find.text('descrizione del prodotto di prova, nulla di più'));

    await delay(4000);

    final price = find.byValueKey(Keys.productPriceField);

    await driver.scrollUntilVisible(productScroll, price, dyScroll: -100.0);

    await driver.waitFor(price);
    await delay(750); // for video capture
    await driver.tap(price);

    await driver.enterText('1234.12,4567');
    await delay(750);
    expect(await driver.getText(price), "123412,45");
    await delay(2000);

    final quantity = find.byValueKey(Keys.productQuantityField);

    await driver.scrollUntilVisible(quantity, price, dyScroll: -100.0);

    await driver.waitFor(quantity);
    await delay(750); // for video capture
    await driver.tap(quantity);

    await driver.enterText('-12.6,8');
    await delay(750);
    expect(await driver.getText(quantity), "1268");
    await delay(2000);

    await driver.scrollUntilVisible(productScroll, buttonAddProductToMyShop,
        dyScroll: 100.0);

    await delay(500);

    await driver.tap(buttonAddProductToMyShop);
    await delay(300);

    await driver.scrollUntilVisible(productScroll, productName,
        dyScroll: 100.0);
  });

  test('Aggiungi prodotto completo', () async {
    // ho già i campi testuali

    final productScroll = find.byValueKey(Keys.productScroll);
    final buttonAddProductToMyShop =
        find.byValueKey(Keys.buttonAddProductToMyShop);
    final customImageViewer =
        find.byValueKey(Keys.customImageViewerAddProductToMyShop);

    await driver.scrollUntilVisible(productScroll, customImageViewer,
        dyScroll: 100.0);

    await delay(1000);
    final image1 = find.byValueKey("addProductPicture__1"); // dynamic
    await driver.waitFor(image1);
    await delay(300);
    driver.tap(image1);
    final gallery = find.byValueKey(Keys.openGallery);
    await driver.waitFor(gallery);
    await delay(300);
    await driver.tap(gallery);

    await delay(1000);
    final image0 = find.byValueKey("addProductPicture__0"); // dynamic
    await driver.waitFor(image0);
    await delay(300);
    await driver.tap(image0);
    final camera = find.byValueKey(Keys.openCamera);
    await driver.waitFor(camera);

    await delay(300);
    await driver.tap(camera);

    await driver.scrollUntilVisible(productScroll, buttonAddProductToMyShop,
        dyScroll: -100.0);

    await driver.tap(buttonAddProductToMyShop);
    await delay(10000);

    final myShopPage = find.byValueKey(Keys.pageMyShop);
    await driver.waitFor(myShopPage);

    await delay(3000);
  });
}
