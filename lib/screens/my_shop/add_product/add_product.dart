import 'dart:collection';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:raaf_project/models/UserApp.dart';
import 'package:raaf_project/models/Seller.dart';
import 'package:raaf_project/screens/my_shop/add_product/custom_image_viewer.dart';
import 'package:raaf_project/services/seller.dart';
import 'package:raaf_project/shared/keys.dart';
import 'package:raaf_project/shared/loading.dart';
import 'package:textfield_tags/textfield_tags.dart';

class AddProduct extends StatefulWidget {
  final UserApp user;
  final Seller seller;

  AddProduct({this.user, this.seller});

  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  // product components
  String _name = "";
  String _description = "";
  List<String> _tags = [];
  SplayTreeMap<int, File> _images = SplayTreeMap<int, File>(); // "sparse array"
  double _price = 0;
  int _quantity = 0;

  final _formKey = GlobalKey<FormState>();
  int _descriptionLengthCounter = 0;
  int _maxDescriptionLength = 250;
  String _prettyPrice = "0,00";
  Color imageColorForCheck = Colors.blue;
  final _sellerService = SellerService();
  bool _loading = false;
  // controller for price field
  TextEditingController _priceController = TextEditingController();
  TextEditingController _quantityController = TextEditingController();

  void initState() {
    super.initState();

    _tags.add(widget.seller.activityName);
    _priceController.addListener(_checkPriceFormat);
    _quantityController.addListener(_checkquantityFormat);
  }

  @override
  void dispose() {
    _priceController.dispose();
    _quantityController.dispose();
    super.dispose();
  }

  _checkquantityFormat() {
    String quantityString = _quantityController.text;
    quantityString = quantityString.replaceAll(" ", "");
    quantityString = quantityString.replaceAll("-", "");
    quantityString = quantityString.replaceAll(",", "");
    quantityString = quantityString.replaceAll(".", "");
    setState(() {
      _quantityController.value = _quantityController.value.copyWith(
          text: quantityString,
          selection: TextSelection(
              baseOffset: quantityString.length,
              extentOffset: quantityString.length));

      _quantity = int.tryParse(quantityString);
    });
  }

  _checkPriceFormat() {
    String _formatEuro(String euro) {
      if (euro.length > 3) {
        int posizioneFine = euro.length;
        while (posizioneFine > 3) {
          posizioneFine -= 3;
          euro = euro.substring(0, posizioneFine) +
              "." +
              euro.substring(posizioneFine);
        }
      }
      return euro;
    }

    String priceString = _priceController.text;
    String prettyPriceString = "0,00";

    priceString = priceString.replaceAll(".", "");
    priceString = priceString.replaceAll("-", "");
    priceString = priceString.replaceAll(" ", "");

    if (priceString.length > 0 &&
        (priceString[0] == "," || priceString[0] == ".")) {
      priceString = priceString.substring(1);
    }

    if (priceString.length > 0) {
      int commaPosition = priceString.indexOf(",");
      int priceLength = priceString.length;
      String prettyEuro = "";
      String euro = "";
      String cents = "";

      commaPosition = commaPosition > 0 ? commaPosition : priceLength;

      euro = priceString.substring(0, commaPosition);

      if (euro.length > 3) {
        prettyEuro = _formatEuro(euro);
      } else
        prettyEuro = euro;

      cents = priceString.substring(commaPosition, priceLength);

      priceLength = cents.length;
      euro = priceLength > 0 ? euro + "," : euro;
      prettyEuro = priceLength > 1 ? prettyEuro + "," : prettyEuro;

      cents = priceLength > 0 ? cents.replaceAll(",", "") : cents; //no comma
      priceLength = cents.length;
      cents = priceLength > 2 ? cents.substring(0, 2) : cents; //only cents(,00)

      priceString = euro + cents;
      prettyPriceString = cents.length > 0 ? prettyEuro + cents : prettyEuro;
    }

    setState(() {
      _priceController.value = _priceController.value.copyWith(
          text: priceString,
          selection: TextSelection(
              baseOffset: priceString.length,
              extentOffset: priceString.length));

      _prettyPrice = prettyPriceString;
      _price = double.tryParse(priceString.replaceAll(",", "."));
    });
  }

  _checkPresenceOfImages() {
    _images.removeWhere((key, value) => value == null);

    if (_images.isEmpty) {
      setState(() {
        imageColorForCheck = Colors.red;
      });
      return false;
    } else {
      setState(() {
        imageColorForCheck = Colors.blue;
      });
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return _loading
        ? Loading()
        : Scaffold(
            key: Key(Keys.pageAddProduct),
            appBar: AppBar(
              title: Text("Aggiunta prodotto"),
              leading: IconButton(
                key: Key(Keys.myShopGoBack),
                icon: Icon(Icons.arrow_back),
                onPressed: () => Navigator.pop(context, null),
              ),
            ),
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: SingleChildScrollView(
                key: Key(Keys.productScroll),
                child: Form(
                  key: _formKey,
                  child: Column(children: [
                    Container(
                        padding: EdgeInsets.only(left: 30, right: 30),
                        child: TextFormField(
                          textAlign: TextAlign.left,
                          key: Key(Keys.productNameField),
                          decoration: InputDecoration(
                            alignLabelWithHint: true,
                            labelText: "Nome prodotto",
                            labelStyle: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.bold),
                          ),
                          validator: (val) => val.isEmpty
                              ? "Aggiungi il nome del prodotto"
                              : null,
                          onChanged: (val) {
                            setState(() {
                              _name = val;
                            });
                          },
                        )),
                    SizedBox(
                      height: 60,
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 30, right: 30),
                        child: TextFieldTags(
                            key: Key(Keys.producTagField),
                            initialTags: <String>[widget.seller.activityName],
                            tagsStyler: TagsStyler(
                                showHashtag: true,
                                tagTextStyle:
                                    TextStyle(fontWeight: FontWeight.bold),
                                tagDecoration: BoxDecoration(
                                  color: Colors.blue[300],
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                                tagCancelIcon: Icon(Icons.cancel,
                                    size: 18.0, color: Colors.blue[900]),
                                tagPadding: const EdgeInsets.all(6.0)),
                            textFieldStyler: TextFieldStyler(
                                textFieldFilled: false,
                                helperText: "Inserisci i tuo tag",
                                hintText: "Nessun tag?"),
                            onTag: (tag) {
                              this._tags.add(tag);
                            },
                            onDelete: (tag) {
                              this._tags.remove(tag);
                            })),
                    SizedBox(
                      height: 60,
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 30, right: 30),
                        child: TextFormField(
                          key: Key(Keys.productDescriptionField),
                          maxLines: 5,
                          keyboardType: TextInputType.multiline,
                          textAlign: TextAlign.left,
                          maxLength: this._maxDescriptionLength,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            alignLabelWithHint: true,
                            labelText: "Descrizione Prodotto",
                            helperText:
                                'Massimo $_maxDescriptionLength Caratteri',
                            counterText:
                                '${this._descriptionLengthCounter}/${this._maxDescriptionLength}',
                            labelStyle: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.bold),
                          ),
                          validator: (val) => val.length < 10
                              ? "Fornisci una descrizione più accurata"
                              : null,
                          onChanged: (val) {
                            setState(() {
                              _description = val;
                              _descriptionLengthCounter = _description.length;
                            });
                          },
                        )),
                    SizedBox(
                      height: 60,
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 30, right: 30),
                        child: TextFormField(
                          controller: _priceController,
                          textAlign: TextAlign.center,
                          keyboardType: TextInputType.number,
                          key: Key(Keys.productPriceField),
                          maxLength: this._maxDescriptionLength,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            alignLabelWithHint: true,
                            labelText: "Prezzo",
                            counterText: "$_prettyPrice €",
                            labelStyle: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.bold),
                          ),
                          validator: (val) =>
                              val.isEmpty ? "Inserisci il prezzo" : null,
                        )),
                    SizedBox(
                      height: 60,
                    ),
                    Container(
                        padding: EdgeInsets.only(left: 30, right: 30),
                        child: TextFormField(
                          controller: _quantityController,
                          textAlign: TextAlign.center,
                          keyboardType: TextInputType.number,
                          key: Key(Keys.productQuantityField),
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            alignLabelWithHint: true,
                            labelText: "Quantita",
                            labelStyle: TextStyle(
                                color: Colors.blue,
                                fontWeight: FontWeight.bold),
                          ),
                          validator: (val) =>
                              val.isEmpty ? "Inserisci la quantità" : null,
                        )),
                    SizedBox(
                      height: 60,
                    ),
                    Text(
                      "Carica alcune immagini",
                      style: TextStyle(
                          color: imageColorForCheck,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 20.0),
                    CustomImageViewer(
                      key: Key(Keys.customImageViewerAddProductToMyShop),
                      imagesPerRow: 2,
                      numberOfImages: 4,
                      onAddedImage: (image, index) {
                        _images[index] = image;
                      },
                      onRemovedImage: (index) {
                        _images[index] = null;
                      },
                    ),
                    ElevatedButton(
                        key: Key(Keys.buttonAddProductToMyShop),
                        onPressed: () async {
                          if (_formKey.currentState.validate() &&
                              _checkPresenceOfImages()) {
                            setState(() => _loading = true);
                            List<File> imagetmp = [];
                            _images.forEach((key, value) {
                              imagetmp.add(value);
                            });
                            dynamic result = await _sellerService.addMyProduct(
                                widget.seller.uid,
                                _name,
                                _description,
                                _tags,
                                _price,
                                _quantity,
                                imagetmp);

                            String message = "Prodotto registrato con successo";
                            if (result == null) {
                              message =
                                  "Errore nella registrazione del prodotto, riprova";
                            }

                            _loading = false;

                            Navigator.pop(context, message);
                          }
                        },
                        child: Text("Aggiungi",
                            style: TextStyle(color: Colors.white))),
                  ]),
                ),
              ),
            ),
          );
  }
}
