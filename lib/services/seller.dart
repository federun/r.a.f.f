import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:raaf_project/models/Seller.dart';

class SellerService {
  final CollectionReference sellerCollection =
      FirebaseFirestore.instance.collection('utente');
  final CollectionReference productsCollection =
      FirebaseFirestore.instance.collection('prodotto');

  final FirebaseStorage productStorage = FirebaseStorage.instance;

  Future<List<String>> _addMyImages(Map<String, File> images) async {
    List<String> urls = [];

    for (MapEntry<String, File> image in images.entries) {
      try {
        Reference firebaseStorageRef =
            productStorage.ref().child("ImmaginiProdotti/${image.key}");
        UploadTask uploadTask = firebaseStorageRef.putFile(image.value);
        urls.add(await (await uploadTask).ref.getDownloadURL());
      } catch (e) {
        print(e.toString());
        return null; // other images will be removed if error occurs, but later
      }
    }
    return urls;
  }

  Future<DocumentReference> addMyProduct(
      String sellerId,
      String name,
      String description,
      List<String> tags,
      num price,
      int quantity,
      List<File> images) async {
    Map<String, File> imageNamesFiles = Map<String, File>();

    RegExp regex = new RegExp(r"[\W]");
    var now = DateTime.now();
    var regexTime = new RegExp(r"[\D]");

    String time = now.toString().replaceAll(regexTime, "");
    int index = 0;
    images.forEach((element) {
      String imageName =
          "${sellerId}__${name.replaceAll(regex, '')}__${time}__$index";
      imageNamesFiles[imageName] = element;
      index++;
    });

    try {
      dynamic urlImages = await _addMyImages(imageNamesFiles);

      dynamic result = await productsCollection.add({
        'venditore': sellerId,
        'nome': name,
        'descrizione': description,
        'tags': tags,
        'prezzo': price,
        'quantita': quantity,
        'immagini': urlImages,
      });

      return result;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Seller _sellerFromFirebase(dynamic sellerDoc, String uid) {
    if (sellerDoc != null) {
      dynamic activity = sellerDoc.get("venditore");
      String companyName = activity["denominazioneSociale"];
      String vatNumber = activity["partitaIva"];
      String address = activity["indirizzo"];
      String activityName = activity["nomeAttivita"];

      return Seller(
          uid: uid,
          activityName: activityName,
          companyName: companyName,
          address: address,
          vatNumber: vatNumber);
    } else
      return null;
  }

  // get information
  Future<Seller> sellerInfo({String uid}) async {
    try {
      dynamic seller = await sellerCollection.doc(uid).get();

      return _sellerFromFirebase(seller, uid);
    } catch (e) {
      print(e.toString());

      return null;
    }
  }
}
