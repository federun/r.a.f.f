class Cart {
  final String uid;
  final Map cartProducts;
  num totalPrice = 0;

  Cart({this.uid, this.cartProducts});
}
