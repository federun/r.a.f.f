import 'package:flutter/material.dart';
import 'package:raaf_project/services/auth.dart';
import 'package:raaf_project/shared/keys.dart';
import 'package:raaf_project/shared/loading.dart';

class Register extends StatefulWidget {
  final Function toggleView;
  Register({this.toggleView});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final AuthService _auth = AuthService();

  final _formKey = GlobalKey<FormState>();
  bool _loading = false;

  String _email = "";
  String _password = "";
  String _error = "";
  String _name = "";
  String _surname = "";
  bool _acceptConditions = false;
  bool _seller = false;

  String _companyName = "";
  String _address = "";
  String _businnessName = "";
  String _vatNumber = "";

  Widget build(BuildContext context) {
    return _loading
        ? Loading()
        : Scaffold(
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: ListView(
                  padding:
                      EdgeInsets.only(top: 100, left: 10, right: 10, bottom: 5),
                  children: [
                    Container(
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                          Text(
                            "Registrati",
                            style: TextStyle(
                                fontSize: 30, color: Color(0xFF35424a)),
                          ),
                          Form(
                            key: _formKey,
                            child: Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 20.0,
                                ),
                                TextFormField(
                                  key: Key(Keys.emailField),
                                  decoration: InputDecoration(
                                    alignLabelWithHint: true,
                                    labelText: "Email",
                                    labelStyle: TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  validator: (val) => val.isEmpty
                                      ? "Inserisci la tua email"
                                      : null,
                                  onChanged: (val) {
                                    setState(() {
                                      _email = val;
                                    });
                                  },
                                ),
                                SizedBox(height: 20.0),
                                TextFormField(
                                  key: Key(Keys.passwordField),
                                  decoration: InputDecoration(
                                    alignLabelWithHint: true,
                                    labelText: "Password",
                                    labelStyle: TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  validator: (val) => val.length < 6
                                      ? "Sono richiesti almeno sei caratteri"
                                      : null,
                                  obscureText: true,
                                  onChanged: (val) {
                                    setState(() {
                                      _password = val;
                                    });
                                  },
                                ),
                                SizedBox(height: 20.0),
                                TextFormField(
                                  key: Key(Keys.confermaPasswordField),
                                  decoration: InputDecoration(
                                    alignLabelWithHint: true,
                                    labelText: "Conferma Password",
                                    labelStyle: TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  validator: (val) => val != _password
                                      ? "Le password non coincidono"
                                      : null,
                                  obscureText: true,
                                ),
                                SizedBox(
                                  height: 20.0,
                                ),
                                TextFormField(
                                  key: Key(Keys.nomeField),
                                  decoration: InputDecoration(
                                    alignLabelWithHint: true,
                                    labelText: "Nome",
                                    labelStyle: TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  validator: (val) {
                                    if (val.isEmpty) {
                                      return "Inserisci il tuo nome";
                                    } else if (val.contains("&")) {
                                      return "il simbolo & non è consentito";
                                    } else
                                      return null;
                                  },
                                  onChanged: (val) {
                                    setState(() {
                                      _name = val;
                                    });
                                  },
                                ),
                                SizedBox(height: 20.0),
                                TextFormField(
                                  key: Key(Keys.cognomeField),
                                  decoration: InputDecoration(
                                    alignLabelWithHint: true,
                                    labelText: "Cognome",
                                    labelStyle: TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  validator: (val) {
                                    if (val.isEmpty) {
                                      return "Inserisci il tuo cognome";
                                    } else if (val.contains("&")) {
                                      return "il simbolo & non è consentito";
                                    } else
                                      return null;
                                  },
                                  onChanged: (val) {
                                    setState(() {
                                      _surname = val;
                                    });
                                  },
                                ),
                                SizedBox(height: 50.0),
                                Row(
                                  children: <Widget>[
                                    Checkbox(
                                      key: Key(Keys.sonoVenditoreFlag),
                                      value: _seller,
                                      onChanged: (bool newValue) {
                                        setState(() {
                                          _seller = newValue;
                                        });
                                      },
                                    ),
                                    Text("Sono un venditore"),
                                  ],
                                ),
                                Visibility(
                                  visible: _seller,
                                  child: SizedBox(
                                    height: 20,
                                  ),
                                ),
                                Visibility(
                                  visible: _seller,
                                  child: TextFormField(
                                    key: Key(Keys.denominazioneSocialeField),
                                    decoration: InputDecoration(
                                      alignLabelWithHint: true,
                                      labelText: "denominazione sociale",
                                      labelStyle: TextStyle(
                                          color: Colors.blue,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    validator: (val) => val.isEmpty
                                        ? "Inserisci la denominazione sociale della tua attività"
                                        : null,
                                    onChanged: (val) {
                                      setState(() {
                                        _companyName = val;
                                      });
                                    },
                                  ),
                                ),
                                Visibility(
                                  visible: _seller,
                                  child: SizedBox(
                                    height: 20,
                                  ),
                                ),
                                Visibility(
                                  visible: _seller,
                                  child: TextFormField(
                                    key: Key(Keys.indirizzoField),
                                    decoration: InputDecoration(
                                      alignLabelWithHint: true,
                                      labelText: "indirizzo",
                                      labelStyle: TextStyle(
                                          color: Colors.blue,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    validator: (val) => val.isEmpty
                                        ? "Inserisci l'indirizzo dell'attività"
                                        : null,
                                    onChanged: (val) {
                                      setState(() {
                                        _address = val;
                                      });
                                    },
                                  ),
                                ),
                                Visibility(
                                  visible: _seller,
                                  child: SizedBox(
                                    height: 20,
                                  ),
                                ),
                                Visibility(
                                  visible: _seller,
                                  child: TextFormField(
                                    key: Key(Keys.nomeAttivitaField),
                                    decoration: InputDecoration(
                                      alignLabelWithHint: true,
                                      labelText: "nome attività",
                                      labelStyle: TextStyle(
                                          color: Colors.blue,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    validator: (val) => val.isEmpty
                                        ? "Inserisci il nome della tua attività"
                                        : null,
                                    onChanged: (val) {
                                      setState(() {
                                        _businnessName = val;
                                      });
                                    },
                                  ),
                                ),
                                Visibility(
                                  visible: _seller,
                                  child: SizedBox(
                                    height: 20,
                                  ),
                                ),
                                Visibility(
                                  visible: _seller,
                                  child: TextFormField(
                                    key: Key(Keys.partitaIvaField),
                                    decoration: InputDecoration(
                                      alignLabelWithHint: true,
                                      labelText: "partita iva",
                                      labelStyle: TextStyle(
                                          color: Colors.blue,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    validator: (val) => val.isEmpty
                                        ? "Inserisci il codice della partita iva"
                                        : null,
                                    onChanged: (val) {
                                      setState(() {
                                        _vatNumber = val;
                                      });
                                    },
                                  ),
                                ),
                                SizedBox(height: 50.0),
                                Row(
                                  children: <Widget>[
                                    Checkbox(
                                      key: Key(Keys.accettoCondizioniFlag),
                                      value: _acceptConditions,
                                      onChanged: (bool newValue) {
                                        setState(() {
                                          _acceptConditions = newValue;
                                        });
                                      },
                                    ),
                                    Text("Accetto i "),
                                    Text("Termini ",
                                        style: TextStyle(
                                          color: Colors.blue,
                                          fontWeight: FontWeight.bold,
                                        )),
                                    Text("e la "),
                                    Text(
                                      "Politica sulla Privacy ",
                                      style: TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 50,
                                ),
                                Visibility(
                                    key: Key(Keys.iscrivitiButton),
                                    visible: _acceptConditions,
                                    child: ElevatedButton(
                                        onPressed: () async {
                                          if (_formKey.currentState
                                              .validate()) {
                                            setState(() => _loading = true);
                                            dynamic result = await _auth
                                                .registerWithEmailAndPassword(
                                              _email,
                                              _password,
                                              _name,
                                              _surname,
                                              _seller,
                                              _companyName,
                                              _address,
                                              _businnessName,
                                              _vatNumber,
                                            );
                                            if (result == null) {
                                              setState(() {
                                                _error =
                                                    "Email non valida oppure già utilizzata";
                                                _loading = false;
                                              });
                                            }
                                          }
                                        },
                                        child: Text("Continua",
                                            style: TextStyle(
                                                color: Colors.white)))),
                                SizedBox(height: 12.0),
                                Text(_error,
                                    style: TextStyle(
                                        color: Colors.red, fontSize: 14.0))
                              ],
                            ),
                          ),
                        ]))
                  ]),
            ),
            bottomNavigationBar: Container(
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                  Text("Sei già registrato?"),
                  TextButton(
                      onPressed: () {
                        widget
                            .toggleView(); // not this (_RegisterState). It's flutter behaviour
                      },
                      child: Text(
                        'Accedi',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15),
                      )),
                ])),
          );
  }
}
