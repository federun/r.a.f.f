/*
Appbar used in home page and in product details.
Used in home and in product details.
*/

import 'package:flutter/material.dart';

class RaffAppBar extends StatefulWidget implements PreferredSizeWidget {
  final String pageKey;
  final num appBarHeight;
  final Color appBarColor;

  RaffAppBar({this.pageKey, this.appBarHeight, this.appBarColor});

  @override
  _RaffAppBarState createState() => _RaffAppBarState();

  @override
  Size get preferredSize => new Size.fromHeight(appBarHeight);
}

class _RaffAppBarState extends State<RaffAppBar> {
  Text _titleField = Text(
    "R.A.F.F.",
    style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
  );

  TextField _searchField = TextField(
      style: TextStyle(
          fontSize: 16, fontWeight: FontWeight.bold, color: Colors.white),
      decoration: InputDecoration(
        hoverColor: Colors.blue,
        fillColor: Colors.blue,
        focusedBorder: null,
        hintText: "Cerca vestiti...",
        border: InputBorder.none,
        hintStyle: TextStyle(color: Colors.white),
      ));

  Icon search = Icon(Icons.search);
  bool searchValue = false;

  void _changeIcon() {
    setState(() {
      if (search.icon == Icons.search) {
        search = Icon(Icons.cancel);
        searchValue = true;
      } else {
        search = Icon(Icons.search);
        searchValue = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      key: Key(widget.pageKey),
      toolbarHeight: widget.appBarHeight,
      backgroundColor: widget.appBarColor,
      elevation: 0.0,
      title: Padding(
          padding: EdgeInsets.only(left: 10),
          child: searchValue ? _searchField : _titleField),
      actions: <Widget>[
        IconButton(icon: search, onPressed: _changeIcon),
        IconButton(
          icon: Icon(Icons.camera_alt_rounded),
          onPressed: () => null,
        )
      ],
    );
  }
}
