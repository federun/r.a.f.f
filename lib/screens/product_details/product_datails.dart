import 'package:flutter/material.dart';
import 'package:raaf_project/models/UserApp.dart';
import 'package:raaf_project/models/Product.dart';
import 'package:raaf_project/screens/product_details/carroussel.dart';
import 'package:raaf_project/services/shoppingcart.dart';
import 'package:raaf_project/shared/app_bar.dart';
import 'package:raaf_project/shared/drawer.dart';
import 'package:raaf_project/shared/keys.dart';

class ProductDetails extends StatefulWidget {
  static String routeName = "/ProductDetails";

  final UserApp user;
  final Product product;
  ProductDetails({this.user, this.product});

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  final CartService _cart = CartService();
  String prettyPrice;

  String showPrettyPrice(num price) {
    String tmp = price.toString();
    tmp = tmp.replaceAll(".", ",");

    // comma
    if (tmp.contains(",") && tmp.split(",")[1].length == 1) {
      tmp = tmp + "0";
    } else if (tmp.contains(",") && tmp.split(",")[1].length == 0) {
      tmp = tmp + ",00";
    }

    // thousands
    if (tmp.split(",")[0].length > 3) {
      for (var i = tmp.split(",")[0].length - 3; i > 0; i -= 3) {
        tmp = tmp.substring(0, i) + "." + tmp.substring(i);
      }
    }
    return tmp;
  }

  _addProductToShoppingCart(Product product) {
    String productId = product.id;
    print(productId);
    _cart.addToCart(widget.user.uid, productId);
  }

  Widget build(BuildContext context) {
    prettyPrice = showPrettyPrice(widget.product.price);

    return Scaffold(
      appBar: RaffAppBar(
        pageKey: Keys.pageProductDetails,
        appBarHeight: 60.0,
        appBarColor: Colors.blue,
      ),
      drawer: RaffDrawer(),
      body: Padding(
        padding: EdgeInsets.fromLTRB(10, 20, 10, 5),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Text(
                widget.product.name,
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  color: Color(0xff232323),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Carroussel(images: widget.product.images),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(40, 0, 15, 0),
                child: Column(
                  children: [
                    Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            prettyPrice + " €",
                            style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.w500,
                              color: Color(0xff232323),
                            ),
                          ),
                          ElevatedButton.icon(
                            key: Key(Keys.productDetailsAddProduct),
                            onPressed: () =>
                                _addProductToShoppingCart(widget.product),
                            icon: Icon(Icons.shopping_cart_outlined),
                            label: Text("Aggiungi al \n Carrello",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                )),
                          ),
                        ]),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        StreamBuilder(
                            stream: _cart.totalInCart(
                                widget.user.uid, widget.product.id),
                            builder: (context, snapShot) {
                              if (!snapShot.hasData || snapShot.hasError) {
                                return Text("");
                              } else {
                                dynamic products =
                                    snapShot.data["prodottiCarrello"];
                                return Text(
                                  products[widget.product.id] == null
                                      ? ""
                                      : "Ne hai ${products[widget.product.id]} nel carrello",
                                  style: TextStyle(
                                    color: Colors.blueGrey,
                                    fontSize: 12,
                                  ),
                                  key: Key(Keys.productDetailsAmountInCart),
                                );
                              }
                            }),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                alignment: Alignment.topLeft,
                child: Text(
                  widget.product.description,
                  style: TextStyle(fontSize: 17),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
