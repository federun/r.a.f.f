import 'package:flutter_driver/flutter_driver.dart';
import 'package:raaf_project/shared/keys.dart';
import 'package:test/test.dart';

void main() {
  FlutterDriver driver;

  Future<void> delay([int milliseconds = 250]) async {
    await Future<void>.delayed(Duration(milliseconds: milliseconds));
  }

  // Connect to the Flutter driver before running any tests.
  setUpAll(() async {
    driver = await FlutterDriver.connect();
  });

  tearDownAll(() async {
    if (driver != null) {
      driver.close();
    }
  });

  test('accedi con credenziali corrette', () async {
    // premo il pulsante di accedi inserendo la password e la mail corretta
    //mi aspetto di arrivare alla home page

    // inserimento email
    final emailField = find.byValueKey(Keys.emailField);
    await driver.waitFor(emailField);
    await delay(750); // for video capture
    await driver.tap(emailField);
    await driver.enterText('aaaa.bb@cc.com');
    await driver.waitFor(find.text('aaaa.bb@cc.com'));

    // inserimento password
    final passwordField = find.byValueKey(Keys.passwordField);
    await driver.waitFor(passwordField);
    await delay(750); // for video capture
    await driver.tap(passwordField);
    await driver.enterText('qwerty123456');
    await driver.waitFor(find.text('qwerty123456'));

    // cerca il bottone di accesso e lo premo
    final signInButton = find.byValueKey(Keys.accediButton);
    await driver.waitFor(signInButton);
    await delay(750); // for video capture
    await driver.tap(signInButton);

    // aspetto 2 secondi e controllo se mi trovo nella home page
    await delay(2000);
    final homePage = find.byValueKey(Keys.homePage);
    await driver.waitFor(homePage);
  });

  // need to rewrite in future
  test('tap temporary button', () async {
    final tmpButton = find.byValueKey(Keys.temporaryButtonForSingleProduct);
    await driver.waitFor(tmpButton);
    await driver.tap(tmpButton);
    await delay(5000);

    final pageProductDetails = find.byValueKey(Keys.pageProductDetails);

    await driver.waitFor(pageProductDetails);
    await delay(300);
  });

  test("images", () async {});

  test('addProductToShop', () async {
    final addProductButton = find.byValueKey(Keys.productDetailsAddProduct);

    await driver.waitFor(addProductButton);
    await delay(750); // for video capture
    await delay(3000);

    final quantity = find.byValueKey(Keys.productDetailsAmountInCart);
    await driver.waitFor(quantity);

    String text = await driver.getText(quantity);

    if (text == "") {
      await driver.tap(addProductButton);
      await delay(5000);
      text = await driver.getText(quantity);
      expect(text, "Ne hai 1 nel carrello");
    } else {
      await driver.tap(addProductButton);
      await delay(1000);
      num oldNumber = int.parse(text.split(" ")[2]);

      text = await driver.getText(quantity);
      num newNumber = int.parse(text.split(" ")[2]);

      if (newNumber > oldNumber) {
        expect(oldNumber, newNumber - 1);
      } else {
        expect(oldNumber, newNumber);
      }
      await delay(3000);
    }
  });
}
