class Keys {
  // list of widget keys that need to be accessed in the test code
  static const String emailField = 'emailField';
  static const String passwordField = 'passwordField';
  static const String accediButton = 'accediButton';
  static const String errorMessage = 'errorMessage';
  static const String logoutButton = 'logoutButton';
  static const String menuLateraleButton = 'menuLateraleButton';
  static const String homePage = 'homePage';
  static const String toIscrivitiPage = 'toIscrivitiPage';
  static const String accettoCondizioniFlag = 'accettoCondizioniFlag';
  static const String iscrivitiButton = 'iscrivitiButton';
  static const String confermaPasswordField = 'confermaPasswordField';
  static const String nomeField = 'nomeField';
  static const String cognomeField = 'cognomeField';
  static const String sonoVenditoreFlag = 'sonoVenditoreFlag';
  static const String denominazioneSocialeField = 'denominazioneSocialeField';
  static const String indirizzoField = 'indirizzoField';
  static const String nomeAttivitaField = 'nomeAttivitaField';
  static const String partitaIvaField = 'partitaIvaField';
  static const String rimuoviAccountButton = 'rimuoviAccountButton';
  static const String continueButton = 'continueButton';
  static const String passwordDimaneticataButton = 'passwordDimaneticataButton';

  // keys in drawer
  static const String drawer = 'drawer';
  static const String myShopDrawer = 'myShopDrawer';
  static const String shoppingCart = 'shoppingCart';

  // list of widgets keys for shop
  static const String pageMyShop = 'pageMyShop';
  static const String buttonAddProduct = 'buttonAddNewProduct';
  static const String pageAddProduct = 'pageAddProduct';
  static const String myShopGoBack = 'myShopGoBack'; // common to all shop pages

  // list of widget keys for shop->add product
  static const String productScroll = 'productScroll';
  static const String productNameField = 'productNameField';
  static const String productDescriptionField = 'productDescriptionField';
  static const String producTagField = 'producTagField';
  static const String productPriceField = 'productPriceField';
  static const String productQuantityField = 'productQuantityField';
  static const String customImageViewerAddProductToMyShop =
      'customImageViewerAddProductToMyShop';

  static const String openCamera = 'openCamera';
  static const String openGallery = 'openGallery';
  static const String buttonAddProductToMyShop = 'buttonAddProductToMyShop';

  // list of widget for product details
  static const String pageProductDetails = 'pageProductDetails';
  static const String productDetailsAmountInCart = 'productDetailsAmountInCart';

  static const String productDetailsAddProduct = 'productDetailsAddProduct';

  // list of widgets keys for cart
  static const String pageShoppingCart = 'pageShoppingCart';
  static const String addToCartButton = 'addToCartButton';
  static const String removeToCartButton = 'removeToCartButton';
  static const String quantity = 'quantity';

  // list fo widget keys for home
  static const String temporaryButtonForSingleProduct =
      'temporaryButtonForSingleProduct';
}
