import 'package:flutter/material.dart';
import 'package:raaf_project/services/auth.dart';
import 'package:raaf_project/shared/keys.dart';
import 'package:raaf_project/shared/loading.dart';

class SignIn extends StatefulWidget {
  final Function toggleView;
  SignIn({this.toggleView});

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final AuthService _auth = AuthService();

  final _formKeyEmail = GlobalKey<FormState>();
  final _formKeyPassword = GlobalKey<FormState>();

  bool _loading = false;

  //text field state
  String _email = "";
  String _password = "";
  String _message = "";
  bool _obscurePassword = true;

  Icon _passwordVisibility = Icon(Icons.visibility);

  void _tooglePasswordVisibility() {
    setState(() {
      _obscurePassword = !_obscurePassword;
      // ignore: unnecessary_statements
      if (_obscurePassword) {
        _passwordVisibility = Icon(Icons.visibility);
      } else {
        _passwordVisibility = Icon(Icons.visibility_off);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return _loading
        ? Loading()
        : Scaffold(
            backgroundColor: Colors.white,
            body: GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.only(
                        top: 50, left: 10, right: 10, bottom: 5),
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 0.0, horizontal: 20.0),
                        child: Column(children: <Widget>[
                          Container(
                            // image here
                            width: 150,
                            height: 150,
                            color: Colors.blue,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 30),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                    child: Text(
                                  "Log In",
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold,
                                      color: Color(0xff35424a)),
                                )),
                                SizedBox(
                                  height: 20.0,
                                ),
                                Form(
                                  key: _formKeyEmail,
                                  child: TextFormField(
                                    key: Key(Keys.emailField),
                                    decoration: InputDecoration(
                                      alignLabelWithHint: true,
                                      labelText: "Email",
                                      labelStyle: TextStyle(
                                          color: Colors.blue,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    validator: (val) => val.isEmpty
                                        ? "Inserisci la tua email"
                                        : null,
                                    onChanged: (val) {
                                      setState(() {
                                        _email = val;
                                      });
                                    },
                                  ),
                                ),
                                SizedBox(height: 20.0),
                                Form(
                                  key: _formKeyPassword,
                                  child: TextFormField(
                                    key: Key(Keys.passwordField),
                                    decoration: InputDecoration(
                                      alignLabelWithHint: true,
                                      labelText: "Password",
                                      suffixIcon: IconButton(
                                          color: Colors.grey,
                                          focusColor: Colors.grey,
                                          icon: _passwordVisibility,
                                          onPressed: _tooglePasswordVisibility),
                                      labelStyle: TextStyle(
                                          color: Colors.blue,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    validator: (val) => val.length < 6
                                        ? "Sono richiesti almeno 6 caratteri"
                                        : null,
                                    obscureText: _obscurePassword,
                                    onChanged: (val) {
                                      setState(() {
                                        _password = val;
                                      });
                                    },
                                  ),
                                ),
                                SizedBox(height: 20.0),
                                ElevatedButton(
                                    key: Key(Keys.accediButton),
                                    onPressed: () async {
                                      setState(() {
                                        _message = "";
                                      });
                                      if ((_formKeyEmail.currentState
                                              .validate()) &
                                          (_formKeyPassword.currentState
                                              .validate())) {
                                        setState(() => _loading = true);
                                        dynamic result = await _auth
                                            .signInWithEmailAndPassword(
                                                _email, _password);
                                        if (result == null) {
                                          setState(() {
                                            _message =
                                                "Impossibile accedere con queste credenziali";
                                            _loading = false;
                                          });
                                        }
                                      }
                                    },
                                    child: Text("Accedi",
                                        style: TextStyle(color: Colors.white))),
                                SizedBox(height: 12.0),
                                Text(_message,
                                    key: Key(Keys.errorMessage),
                                    style: TextStyle(
                                        color: Colors.red, fontSize: 14.0))
                              ],
                            ),
                          )
                        ])),
                  ),
                )),
            bottomNavigationBar: Container(
                height: 56,
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 0),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Container(
                        child: TextButton(
                            key: Key(Keys.passwordDimaneticataButton),
                            onPressed: () async {
                              setState(() {
                                _message = "";
                              });
                              if (_formKeyEmail.currentState.validate()) {
                                setState(() => _loading = true);
                                dynamic result =
                                    await _auth.resetPassword(_email);
                                if (result == null) {
                                  setState(() {
                                    _message =
                                        "Questa mail non è associata a nessun account, prova a reinserirla";
                                  });
                                } else {
                                  setState(() {
                                    _message =
                                        "Ti è stata inviata una mail all'indirizzo inserito. Segui le istruzioni contenute in essa, poi esegui il login con le nuove credenziali";
                                  });
                                }
                                setState(() => _loading = false);
                              }
                            },
                            child: Text("Password dimenticata?"))),
                    Container(
                      child: TextButton.icon(
                          key: Key(Keys.toIscrivitiPage),
                          onPressed: () {
                            widget.toggleView();
                          }, // not this (_RegisterState). It's flutter behaviour,
                          icon: Icon(Icons.person),
                          label: Text('Iscriviti')),
                    )
                  ],
                )),
          );
  }
}
