import 'package:flutter_driver/flutter_driver.dart';
import 'package:raaf_project/shared/keys.dart';
import 'package:test/test.dart';

void main() {
  FlutterDriver driver;

  Future<void> delay([int milliseconds = 250]) async {
    await Future<void>.delayed(Duration(milliseconds: milliseconds));
  }

  // Connect to the Flutter driver before running any tests.
  setUpAll(() async {
    driver = await FlutterDriver.connect();
  });

  tearDownAll(() async {
    if (driver != null) {
      driver.close();
    }
  });
  test('accedi con credenziali corrette', () async {
    // premo il pulsante di accedi inserendo la password e la mail corretta
    //mi aspetto di arrivare alla home page

    // inserimento email
    final emailField = find.byValueKey(Keys.emailField);
    await driver.waitFor(emailField);
    await delay(750); // for video capture
    await driver.tap(emailField);
    await driver.enterText('prova@gmail.com');
    await driver.waitFor(find.text('prova@gmail.com'));

    // inserimento password
    final passwordField = find.byValueKey(Keys.passwordField);
    await driver.waitFor(passwordField);
    await delay(750); // for video capture
    await driver.tap(passwordField);
    await driver.enterText('password');
    await driver.waitFor(find.text('password'));

    // cerca il bottone di accesso e lo premo
    final signInButton = find.byValueKey(Keys.accediButton);
    await driver.waitFor(signInButton);
    await delay(750); // for video capture
    await driver.tap(signInButton);

    // aspetto 2 secondi e controllo se mi trovo nella home page
    await delay(2000);
    final homePage = find.byValueKey(Keys.homePage);
    await driver.waitFor(homePage);
  });

  test('Open drawer and go to shopping cart page', () async {
    final drawer = find.byTooltip('Open navigation menu');
    await driver.waitFor(drawer);
    await driver.tap(drawer);
    await delay(1000);
    final shoppingCart = find.byValueKey(Keys.shoppingCart);
    await driver.waitFor(shoppingCart);
    await delay(300);
    driver.tap(shoppingCart);
    await delay(5000);
    final myShopPage = find.byValueKey(Keys.pageShoppingCart);
    await driver.waitFor(myShopPage);
  });


  /* The cart must have only one product! (all the widget have the same key) */
  test('increase product quantity', () async {
    final addProduct = find.byValueKey(Keys.addToCartButton);
    await driver.waitFor(addProduct);
    await driver.tap(addProduct);
    await delay(5000);
  });

  test('decrease product quantity', () async {
    final addProduct = find.byValueKey(Keys.removeToCartButton);
    await driver.waitFor(addProduct);
    await driver.tap(addProduct);
    await delay(5000);
  });


}

