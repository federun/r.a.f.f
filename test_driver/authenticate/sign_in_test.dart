// Imports the Flutter Driver API.
import 'dart:io';

import 'package:flutter_driver/flutter_driver.dart';
import 'package:raaf_project/shared/keys.dart';
import 'package:test/test.dart';

Future<void> main() async {
  FlutterDriver driver;
  Future<void> delay([int milliseconds = 250]) async {
    await Future<void>.delayed(Duration(milliseconds: milliseconds));
  }

  // Connect to the Flutter driver before running any tests.
  setUpAll(() async {
    driver = await FlutterDriver.connect();
  });

  tearDownAll(() async {
    if (driver != null) {
      driver.close();
    }
  });
  test('check flutter driver health', () async {
    final health = await driver.checkHealth();
    expect(health.status, HealthStatus.ok);
  });

  group('reset password', () {
    final String userMail =
        ''; //inserire una email valida qui NON presente nel db, verrà creato un account, inviata a questo indirizzo una mail e successivamente rimosso tale account
    if (userMail == "") {
      print(
          "ATTENZIONE: inserisci una mail in group: 'reset password', variabile: 'userMail' prima di lanciare il codice");
      exit(0);
    }
    final String userPassword = 'aaaaaa';
    final String userName = 'Mario';
    final String userSurname = 'Rossi';

    Future premiFlag(SerializableFinder flag) async {
      await driver.waitFor(flag);
      await driver.scrollIntoView(flag);
      await driver.tap(flag);
    }

    Future pressButton(
        SerializableFinder iscrivitiButton, bool iscrizioneOk) async {
      await driver.waitFor(iscrivitiButton);
      await driver.scrollIntoView(iscrivitiButton);
      // premo pulsante di iscrizione
      await driver.tap(iscrivitiButton);
      await delay(5000);
      if (iscrizioneOk == false) {
        // aspetto 5 secondi e controllo se trovo ancora il pulsante di iscrizione
        await driver.waitFor(iscrivitiButton);
      } else {
        // aspetto 5 secondi e controllo se mi trovo nella home page
        final homePage = find.byValueKey(Keys.homePage);
        await driver.waitFor(homePage);
      }
    }

    Future insertText(SerializableFinder field, String testo) async {
      await driver.waitFor(field);
      await driver.scrollIntoView(field);
      await driver.tap(field);
      await driver.enterText(testo);
      await driver.waitFor(find.text(testo));
    }

    Future eliminaAccount(bool accesso) async {
      if (accesso == false) {
        // inserimento email
        final emailField = find.byValueKey(Keys.emailField);
        await insertText(emailField, userMail);

        // inserimento password
        final passwordField = find.byValueKey(Keys.passwordField);
        await insertText(passwordField, userPassword);

        // cerca il bottone di accesso e lo premo
        final accediButton = find.byValueKey(Keys.accediButton);
        await pressButton(accediButton, true);
      }
      final drawer = find.byTooltip('Open navigation menu');
      await driver.tap(drawer);

      final rimuoviAccountButton = find.byValueKey(Keys.rimuoviAccountButton);
      await driver.tap(rimuoviAccountButton);

      final passwordField = find.byValueKey(Keys.passwordField);
      await insertText(passwordField, userPassword);

      final continueButton = find.byValueKey(Keys.continueButton);
      await driver.waitFor(continueButton);
      await delay(2000);
      await driver.tap(continueButton);
    }

    test('password dimenticata senza inserire alcuna mail', () async {
      //premo password dimenticata senza inserire nulla
      //mi aspetto di trovare il messaggio: ""

      final passwordDimaneticataButton =
          find.byValueKey(Keys.passwordDimaneticataButton);
      await pressButton(passwordDimaneticataButton, false);

      final errorMessage = find.byValueKey(Keys.errorMessage);
      expect(await driver.getText(errorMessage), "");
    });
    test('password dimenticata di account insesistente', () async {
      //premo password dimenticata dopo aver inserito una mail non presente nel db
      //mi aspetto di trovare il messaggio: "Questa mail non è associata a nessun account, prova a reinserirla"

      final emailField = find.byValueKey(Keys.emailField);
      await insertText(emailField, userMail);

      final passwordDimaneticataButton =
          find.byValueKey(Keys.passwordDimaneticataButton);
      await pressButton(passwordDimaneticataButton, false);

      final errorMessage = find.byValueKey(Keys.errorMessage);
      expect(await driver.getText(errorMessage),
          "Questa mail non è associata a nessun account, prova a reinserirla");
    });

    test('password dimenticata di account presente nel db', () async {
      //premo password dimenticata dopo aver inserito una mail presente nel db
      //mi aspetto di trovare il messaggio: "Ti è stata inviata una mail all'indirizzo inserito. Segui le istruzioni contenute in essa, poi esegui il login con le nuove credenziali"

      //prima devo creare l'account

      // passo alla pagina di registrazione
      final toIscrivitiPage = find.byValueKey(Keys.toIscrivitiPage);
      await driver.waitFor(toIscrivitiPage);
      await driver.tap(toIscrivitiPage);

      // compilo tutti i campi correttamente
      final emailField = find.byValueKey(Keys.emailField);
      await insertText(emailField, userMail);

      final passwordField = find.byValueKey(Keys.passwordField);
      await insertText(passwordField, userPassword);

      final confermaPasswordField = find.byValueKey(Keys.confermaPasswordField);
      await insertText(confermaPasswordField, userPassword);

      final nomeField = find.byValueKey(Keys.nomeField);
      await insertText(nomeField, userName);

      final cognomeField = find.byValueKey(Keys.cognomeField);
      await insertText(cognomeField, userSurname);

      //accetto le condizioni
      final accettoCondizioniFlag = find.byValueKey(Keys.accettoCondizioniFlag);
      await premiFlag(accettoCondizioniFlag);

      // cerca il bottone di iscrizione
      final iscrivitiButton = find.byValueKey(Keys.iscrivitiButton);
      await pressButton(iscrivitiButton, true);

      final drawer = find.byTooltip('Open navigation menu');
      await driver.tap(drawer);

      final logoutButton = find.byValueKey(Keys.logoutButton);
      await driver.tap(logoutButton);

      //qui torno nella pagina di sign in, avendo l'account registrato

      //final emailField = find.byValueKey(Keys.emailField);
      await insertText(emailField, userMail);

      final passwordDimaneticataButton =
          find.byValueKey(Keys.passwordDimaneticataButton);
      await pressButton(passwordDimaneticataButton, false);

      final errorMessage = find.byValueKey(Keys.errorMessage);
      expect(await driver.getText(errorMessage),
          "Ti è stata inviata una mail all'indirizzo inserito. Segui le istruzioni contenute in essa, poi esegui il login con le nuove credenziali");

      // test completato, rimuovo l'utente per poter rilanciare il test
      await eliminaAccount(false);
    }, timeout: Timeout(Duration(minutes: 2)));
  });
  group('login', () {
    final String userMail = 'email@email.com';
    final String userPassword = 'aaaaaa';

    Future premiAccedi(SerializableFinder accediButton, bool accediOk) async {
      await driver.waitFor(accediButton);
      // piccolo ritardo per visualizzare la demo
      await delay(750);
      // premo pulsante di accesso
      await driver.tap(accediButton);
      await delay(5000);
      if (accediOk == false) {
        // aspetto 5 secondi e controllo se trovo ancora il pulsante si accesso
        await driver.waitFor(accediButton);
      } else {
        // aspetto 5 secondi e controllo se mi trovo nella home page
        final homePage = find.byValueKey(Keys.homePage);
        await driver.waitFor(homePage);
      }
    }

    Future insertText(SerializableFinder field, String text) async {
      await driver.waitFor(field);
      await delay(750); // for video capture
      await driver.tap(field);
      await driver.enterText(text);
      await driver.waitFor(find.text(text));
    }

    test('accedi con campi vuoti', () async {
      // premo il pulsante di accedi senza inserire campi
      //mi aspetto di rimanere nella pagina di accesso

      // cerca il bottone di accesso
      final accediButton = find.byValueKey(Keys.accediButton);
      bool accediOk = false;
      await premiAccedi(accediButton, accediOk);
    });

    test('accedi con solo email', () async {
      // premo il pulsante di accedi inserendo solo la mail
      //mi aspetto di rimanere nella pagina di accesso

      // inserimento email
      final emailField = find.byValueKey(Keys.emailField);
      await insertText(emailField, userMail);

      // cerca il bottone di accesso e lo premo
      final accediButton = find.byValueKey(Keys.accediButton);
      await premiAccedi(accediButton, false);

      //pulisco casella email
      await insertText(emailField, '');
    });

    test('accedi con solo password', () async {
      // premo il pulsante di accedi inserendo solo la password
      //mi aspetto di rimanere nella pagina di accesso
      final passwordField = find.byValueKey(Keys.passwordField);
      await insertText(passwordField, userPassword);

      // cerca il bottone di accesso e lo premo
      final accediButton = find.byValueKey(Keys.accediButton);
      await premiAccedi(accediButton, false);

      //pulisco casella password
      await insertText(passwordField, '');
    });

    test('accedi con credenziali sbagliate', () async {
      // premo il pulsante di accedi inserendo la password e/o la mail sbagliata
      //mi aspetto di rimanere nella pagina di accesso

      // inserimento email
      final emailField = find.byValueKey(Keys.emailField);
      await insertText(emailField, userMail);

      // inserimento password sbagliata
      final passwordField = find.byValueKey(Keys.passwordField);
      await insertText(passwordField, 'passwordUsataDaNessuno');

      // cerca il bottone di accesso e lo premo
      final accediButton = find.byValueKey(Keys.accediButton);
      await premiAccedi(accediButton, false);

      // inserimento email sbagliata
      await insertText(emailField, 'email.non.presente@email.com');

      // inserimento password
      await insertText(passwordField, userPassword);

      // cerca il bottone di accesso e lo premo
      await premiAccedi(accediButton, false);

      // inserimento email sbagliata
      await insertText(emailField, 'email.non.presente@email.com');

      // inserimento password sbagliata
      await insertText(passwordField, 'passwordUsataDaNessuno');

      // cerca il bottone di accesso e lo premo
      await premiAccedi(accediButton, false);
    });

    test('accedi con credenziali corrette', () async {
      // premo il pulsante di accedi inserendo la password e la mail corretta
      //mi aspetto di arrivare alla home page

      // inserimento email
      final emailField = find.byValueKey(Keys.emailField);
      await insertText(emailField, userMail);

      // inserimento password
      final passwordField = find.byValueKey(Keys.passwordField);
      await insertText(passwordField, userPassword);

      // cerca il bottone di accesso e lo premo
      final accediButton = find.byValueKey(Keys.accediButton);
      await premiAccedi(accediButton, true);
    });
  });
// Close the connection to the driver after the tests have completed.
}
