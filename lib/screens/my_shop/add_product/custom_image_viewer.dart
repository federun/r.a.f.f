import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:raaf_project/shared/keys.dart';

class CustomImageViewer extends StatefulWidget {
  final int numberOfImages;
  final int imagesPerRow;
  final void Function(File image, int index) onAddedImage;
  final void Function(int index) onRemovedImage;

  CustomImageViewer({
    Key key,
    @required this.numberOfImages,
    @required this.imagesPerRow,
    @required @required this.onAddedImage,
    @required this.onRemovedImage,
  }) : super(key: key);

  @override
  _CustomImageViewerState createState() => _CustomImageViewerState();
}

class _CustomImageViewerState extends State<CustomImageViewer> {
  List<Object> _images = [];
  final ImagePicker _picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < widget.numberOfImages; ++i) {
      _images.add("Add Image");
    }
  }

  _openGallery(BuildContext context, int index) async {
    Navigator.of(context).pop(); //close dialog
    var picture =
        await _picker.getImage(source: ImageSource.gallery, imageQuality: 50);
    this.setState(() {
      if (picture != null) {
        widget.onAddedImage(File(picture.path), index);
        _images.replaceRange(index, index + 1, [File(picture.path)]);
      }
    });
  }

  _openCamera(
    BuildContext context,
    int index,
  ) async {
    var picture =
        await _picker.getImage(source: ImageSource.camera, imageQuality: 50);
    this.setState(() {
      if (picture != null) {
        widget.onAddedImage(File(picture.path), index);
        _images.replaceRange(index, index + 1, [File(picture.path)]);
      }
    });
    Navigator.of(context).pop(); //close dialog
  }

  Future<void> _showChoiceDialog(BuildContext context, int index) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Scegli sorgente"),
            content: SingleChildScrollView(
                child: ListBody(
              children: <Widget>[
                GestureDetector(
                  key: Key(Keys.openGallery),
                  child: Text("Apri la galleria"),
                  onTap: () {
                    _openGallery(context, index);
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  child: Text("Scatta una foto"),
                  key: Key(Keys.openCamera),
                  onTap: () {
                    _openCamera(context, index);
                  },
                )
              ],
            )),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 400,
        child: GridView.count(
          // physics: NeverScrollableScrollPhysics(),
          crossAxisCount: widget.imagesPerRow,
          childAspectRatio: 1,
          children: List.generate(_images.length, (index) {
            if (_images[index] is File) {
              File uploadModel = _images[index];
              return Card(
                key: Key("addProductPicture__$index"),
                clipBehavior: Clip.antiAlias,
                child: Stack(
                  children: <Widget>[
                    Center(
                        child: Image(
                      image: FileImage(uploadModel),
                      width: 300,
                      height: 300,
                    )),
                    Positioned(
                      right: 5,
                      top: 5,
                      child: InkWell(
                        child: Icon(
                          Icons.remove_circle,
                          size: 30,
                          color: Colors.red,
                        ),
                        onTap: () {
                          setState(() {
                            _images
                                .replaceRange(index, index + 1, ["Add Image"]);
                            widget.onRemovedImage(index);
                          });
                        },
                      ),
                    ),
                  ],
                ),
              );
            } else {
              return Card(
                key: Key("addProductPicture__$index"),
                child: IconButton(
                  icon: Icon(Icons.add),
                  onPressed: () {
                    _showChoiceDialog(context, index);
                  },
                ),
              );
            }
          }),
        ));

    // Image.file(_imageFile, width: 400, height: 400);
  }
}
