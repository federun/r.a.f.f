import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:raaf_project/models/UserApp.dart';
import 'package:raaf_project/screens/home/home.dart';
import 'package:raaf_project/screens/my_purchases/my_purchases.dart';
import 'package:raaf_project/screens/my_shop/my_shop.dart';
import 'package:raaf_project/screens/product_details/product_datails.dart';
import 'package:raaf_project/screens/shopping_cart/shopping_cart.dart';

import 'package:raaf_project/screens/wrapper.dart';

import 'package:firebase_core/firebase_core.dart';
import 'package:raaf_project/services/auth.dart';

//enum AuthServiceType { firebase, mock }

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
/*   // [initialAuthServiceType] is made configurable for testing
  const MyApp({this.initialAuthServiceType = AuthServiceType.firebase});
  final AuthServiceType initialAuthServiceType; */
  @override
  Widget build(BuildContext context) {
    return StreamProvider<UserApp>.value(
        // actrive listeing on stream
        value: AuthService().user, // getter
        initialData: null,
        child: MaterialApp(
          onGenerateRoute: (RouteSettings settings) {
            print('build route for ${settings.name}');
            var routes = <String, WidgetBuilder>{
              Wrapper.routeName: (ctx) => Wrapper(),
              ShoppingCart.routeName: (ctx) =>
                  ShoppingCart(user: settings.arguments),
              Home.routeName: (ctx) => Home(user: settings.arguments),
              MyShop.routeName: (ctx) => MyShop(user: settings.arguments),
              MyPurchases.routeName: (ctx) =>
                  MyPurchases(user: settings.arguments),
              ProductDetails.routeName: (ctx) {
                List<dynamic> args = settings.arguments;
                return ProductDetails(user: args[0], product: args[1]);
              },
            };

            WidgetBuilder builder = routes[settings.name];
            return MaterialPageRoute(builder: (ctx) => builder(ctx));
          },

          /*home: Wrapper(), routes: <String, WidgetBuilder>{
          ShoppingCart.routeName: (BuildContext context) => ShoppingCart(),
          Home.routeName: (BuildContext context) => Home(),
          MyPurchases.routeName: (BuildContext context) => MyPurchases(),
          MyShop.routeName: (BuildContext context) => MyShop(),}*/
        ));
  }
}
