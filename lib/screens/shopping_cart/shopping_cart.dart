import 'package:flutter/material.dart';
import 'package:raaf_project/models/Product.dart';
import 'package:raaf_project/models/UserApp.dart';
import 'package:raaf_project/services/shoppingcart.dart';
import 'package:raaf_project/shared/drawer.dart';
import 'package:raaf_project/shared/keys.dart';

class ShoppingCart extends StatefulWidget {
  static String routeName = "/ShoppingCart";

  final UserApp user;
  ShoppingCart({this.user});

  @override
  _ShoppingCartState createState() => _ShoppingCartState();
}

class _ShoppingCartState extends State<ShoppingCart> {
  final CartService _cartService = CartService();
  bool _endOrderVisible = false;

  List<Widget> productsCardWidgets(
      List<ProductQuantityPair<Product, num>> products, String price) {
    List<Widget> productWidget = <Widget>[];

    //here you can do any processing you need as long as you return a list of ```Widget```.

    for (var pair in products) {
      Product product = pair.product;

      productWidget.add(Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        margin: EdgeInsets.all(10),
        elevation: 4,
        //      color: Color.fromRGBO(64, 75, 96, .9),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Row(
              //   mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.0),
                  child: CircleAvatar(
                      radius: 25,
                      backgroundImage:
                          NetworkImage(product.images[0]) //Product Image
                      ),
                ),
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Container(
                    width: 120,
                    child: Text(product.name,
                        softWrap: false,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.bold)),
                  ), //Product Name

                  Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 5.0, vertical: 4.0),
                      child: Text(
                          (product.price.toStringAsFixed(2))
                                  .replaceAll(".", ",") +
                              ' €',
                          textAlign: TextAlign.left,
                          style: TextStyle(fontSize: 12))),
                ]),
                Spacer(),
                // Button -
                IconButton(
                  key: Key(Keys.removeToCartButton),
                  icon: Icon(
                    Icons.remove_circle,
                    color: Colors.blue,
                  ),
                  splashRadius: 20,
                  onPressed: () async => {
                    await _cartService.removeToCart(
                        widget.user.uid, product.id),
                  },
                ),
                // Quantity
                Text(
                  pair.quantity.toString(), //CART quantity
                  style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                  key: Key(Keys.quantity),
                ),
                // Button +
                IconButton(
                  key: Key(Keys.addToCartButton),
                  splashRadius: 20,
                  icon: Icon(
                    Icons.add_circle,
                    color: Colors.blue,
                  ),
                  onPressed: () async => {
                    await _cartService.addToCart(widget.user.uid, product.id),
                  },
                ),
              ]),
        ),
      ));
    }

    productWidget.add(
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              // Quantity
              Text(
                "Totale", //CART quantity
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                key: Key(Keys.quantity),
              ),
              Text(price.toString().replaceAll(".", ",") + " €",
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
            ]),
      ),
    );

    productWidget.add(Visibility(
        maintainState: true,
        maintainAnimation: true,
        maintainSize: true,
        visible: _endOrderVisible,
        child: ElevatedButton(
            onPressed: () => {}, child: Text("Concludi ordine"))));
    return productWidget;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            key: Key(Keys.pageShoppingCart),
            toolbarHeight: 60,
            backgroundColor: Colors.blue,
            elevation: 0.0,
            title: Text("Cart")),
        drawer: RaffDrawer(),
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Center(
            child: Column(children: [
              StreamBuilder(
                  stream: _cartService.getCart(uid: widget.user.uid),
                  builder: (context, asyncSnapshot) {
                    //no check, a future will ever be there
                    return FutureBuilder(
                        future: asyncSnapshot.data,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            // but future will have data?? 🤔 :-P
                            num price = 0;
                            List<ProductQuantityPair<Product, num>> products =
                                snapshot.data;
                            for (var tmp in products) {
                              price += tmp.quantity * tmp.product.price;
                            }
                            if (price > 0) {
                              _endOrderVisible = true;
                            } else
                              _endOrderVisible = false;

                            return Column(
                              children: productsCardWidgets(
                                  products, price.toStringAsFixed(2)),
                            );
                          } else {
                            return CircularProgressIndicator();
                          }
                        });
                  }),
            ]),
          ),
        ));
  }
}
