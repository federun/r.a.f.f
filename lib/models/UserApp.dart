class UserApp {
  final String uid;
  final String email;
  final String nome;
  final String cognome;

  UserApp({this.uid, this.email, this.nome, this.cognome});
}
