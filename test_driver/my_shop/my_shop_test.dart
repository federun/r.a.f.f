import 'package:flutter_driver/flutter_driver.dart';
import 'package:raaf_project/shared/keys.dart';
import 'package:test/test.dart';

void main() {
  FlutterDriver driver;

  Future<void> delay([int milliseconds = 250]) async {
    await Future<void>.delayed(Duration(milliseconds: milliseconds));
  }

  // Connect to the Flutter driver before running any tests.
  setUpAll(() async {
    driver = await FlutterDriver.connect();
  });

  tearDownAll(() async {
    if (driver != null) {
      driver.close();
    }
  });

  test('check flutter driver health', () async {
    final health = await driver.checkHealth();
    expect(health.status, HealthStatus.ok);
  });

  test('accedi con credenziali corrette', () async {
    // premo il pulsante di accedi inserendo la password e la mail corretta
    //mi aspetto di arrivare alla home page

    // inserimento email
    final emailField = find.byValueKey(Keys.emailField);
    await driver.waitFor(emailField);
    await delay(750); // for video capture
    await driver.tap(emailField);
    await driver.enterText('l.mendozzi@campus.unimib.it');
    await driver.waitFor(find.text('l.mendozzi@campus.unimib.it'));

    // inserimento password
    final passwordField = find.byValueKey(Keys.passwordField);
    await driver.waitFor(passwordField);
    await delay(750); // for video capture
    await driver.tap(passwordField);
    await driver.enterText('qwerty123456');
    await driver.waitFor(find.text('qwerty123456'));

    // cerca il bottone di accesso e lo premo
    final signInButton = find.byValueKey(Keys.accediButton);
    await driver.waitFor(signInButton);
    await delay(750); // for video capture
    await driver.tap(signInButton);

    // aspetto 5 secondi e controllo se mi trovo nella home page
    await delay(5000);
    final homePage = find.byValueKey(Keys.homePage);
    await driver.waitFor(homePage);
  });

  test('Open drawer and go to my shop page', () async {
    final drawer = find.byTooltip('Open navigation menu');
    await driver.waitFor(drawer);
    await driver.tap(drawer);
    await delay(1000);
    final myShop = find.byValueKey(Keys.myShopDrawer);
    await driver.waitFor(myShop);
    await delay(300);
    driver.tap(myShop);
    await delay(5000);
    final myShopPage = find.byValueKey(Keys.pageMyShop);
    await driver.waitFor(myShopPage);
  });

  test('Open add product page and go back', () async {
    final addNewProduct = find.byValueKey(Keys.buttonAddProduct);
    await driver.waitFor(addNewProduct);
    await driver.tap(addNewProduct);
    await delay(1000);

    final pageMyProduct = find.byValueKey(Keys.pageAddProduct);
    await driver.waitFor(pageMyProduct);
    await delay(1000);

    final myShopGoBack = find.byValueKey(Keys.myShopGoBack);
    await driver.waitFor(myShopGoBack);
    driver.tap(myShopGoBack);
    await delay(1000);

    final myShopPage = find.byValueKey(Keys.pageMyShop);
    await driver.waitFor(myShopPage);
  });
}
