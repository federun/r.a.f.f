import 'package:firebase_auth/firebase_auth.dart';
import 'package:raaf_project/models/UserApp.dart';
import 'package:raaf_project/services/usercollection.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // create user obj based on Firebaseuser
  UserApp _userFromFirebaseUser(User user) {
    try {
      return user != null
          ? UserApp(
              uid: user.uid,
              email: user.email,
              nome: user.displayName.split('&')[0] ?? '',
              cognome: user.displayName.split('&')[1] ?? '')
          : null;
    } catch (e) {
      return user != null
          ? UserApp(uid: user.uid, email: user.email, nome: ' ', cognome: ' ')
          : null;
    }
  }

  // auth change user stream
  Stream<UserApp> get user {
    return _auth.userChanges().map((User user) => _userFromFirebaseUser(user));
  }

  // sing in anonymously
  // Future signInAnon() async {
  //   try {
  //    UserCredential result = await _auth.signInAnonymously();
  //    User user = result.user;
  //    return _userFromFirebaseUser(user);
  //  } catch (e) {
  //    print(e.toString());
  //    return null;
  //
  //  }

  // sign in with email & password
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      User user = result.user;

      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // register with email & password
  Future registerWithEmailAndPassword(
    String email,
    String password,
    String nome,
    String cognome,
    bool venditore,
    String denominazioneSociale,
    String indirizzo,
    String nomeAttivita,
    String partitaIva,
  ) async {
    try {
      UserCredential result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);

      //await result.user.updateProfile(displayName: nome + '&' + cognome);
      User user = result.user;

      user.updateProfile(displayName: nome + '&' + cognome).then((value) {
        user = _auth.currentUser;
      });

      //create new document for the user with the uid
      await DatabaseService(uid: user.uid).insertUserData(
        venditore,
        denominazioneSociale,
        indirizzo,
        nomeAttivita,
        partitaIva,
      );

      //await signOut();
      //await signInWithEmailAndPassword(email, password);
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //sign out
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future deleteUser(String email, String password) async {
    try {
      User user = _auth.currentUser;
      AuthCredential credentials =
          EmailAuthProvider.credential(email: email, password: password);
      UserCredential result =
          await user.reauthenticateWithCredential(credentials);
      await DatabaseService(uid: result.user.uid)
          .deleteuser(); // called from database class
      await result.user.delete();
      return true;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future resetPassword(String email) async {
    try {
      await _auth.sendPasswordResetEmail(email: email);
      return true;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
